<?php

checkLogin(); 
function index(){
    view('admin','banner','index',array('title'=>'Banner设置'));
}

function gettab(){
    $arr=array('首页','关于我们','新闻中心','产品中心','联系我们');
    $data=[];
    foreach($arr as $v){
        $result=find('settab','id,value',array('name'=>$v));        
        if($arr){
            $data[$v]=$result['value'];
        }
    }   
    view('admin','banner','gettab',$data,false);
}

function edit(){

    if(!empty($_GET['name'])){        
        $row=find('settab','',array('name'=>$_GET['name']));
        if(!empty($_POST)){
			$imgarr=array();		
			
			//先上传添加的图片
			if(isset( $_POST['checkimages'])){			
				$imgs=uploadImg('images', 'upload/banner', $_POST['checkimages'],'',false);		 
				if(empty($imgs['error'])){
					$imgarr= array_merge($imgarr,explode('|',$imgs['image']));
					
				}else{
					echo $imgs['error'];
					die;
				}			 
			}  
			//删除替换掉的图片
			$needimgarr=[];
			if(!empty($_POST['uploadedimages'])){
				$needimgarr=$_POST['uploadedimages'];
			}
			 
			if($row){
				$updimgarr=explode('|',$row['value']);
				
				for($i=0;$i<count($updimgarr);$i++){
					if(in_array($i,$needimgarr)){
						//修改后有此图片
						$imgarr[]=$updimgarr[$i];
					
					}else{
						//修改后没有此图片
						if(file_exists($updimgarr[$i])){
							unlink($updimgarr[$i]);
						}
						

					}

				}

			}


			$_POST['value']=implode('|',$imgarr);
			
			
			$result=update('settab',array('name'=>$_GET['name']));
			if($result){					
				jump('index',"curpage=$curpage");	
			}else{
				echo "<script>alert('修改没有成功哦！');</script>";			
			}           
        }
        
        view('admin','banner','edit',array('row'=>$row,'title'=>'编辑Banner图'));


    }
}