<?php 

checkLogin();
function index(){
    
  
    view('admin','category','index',array('title'=>'分类管理'));

}
function gettab(){
      
    $arr=getCategory();
     view('admin','category','gettab',array('rows'=>$arr),false);


}

function add(){
     

     if(!empty($_POST)){
         if(insert('category')){
             jump('index');
         }else{
             echo "<script>alert('添加分类没有成功哦！');</script>";
         }
     }  
    $arr=getCategory();
    view('admin','category','add',['title'=>'添加分类','rows'=>$arr]);

}

function edit(){
    

    if(!empty($_GET['id'])){
        $id=$_GET['id'];
        if(!empty($_POST)){
             if(update('category',$id)){
                jump('index');
            }else{
                echo "<script>alert('修改分类没有成功哦！');</script>";
            }
        }
        $row=find('category','',$id);
        $arr=getCategory(0,$id);
        view('admin','category','edit',array('title'=>'分类管理','row'=>$row,'rows'=>$arr));

    }
   
}

function del(){
    if(!empty($_GET['id'])){        
        $idarr=explode('|',$_GET['id']);
        foreach($idarr as $id){
            $row=find('category','',$id);
            $arr=getCategory($id);
            foreach($arr as $v){
                delete('category',$v['id']);
            }
            delete('category',$id);
        }
        jump('index');
    }

}