<?php

function index(){
    checkLogin();

   if(!empty($_POST)){ //判断是否是提交过来的，也即是否为一个post请求

      $data['username'] =  addslashes($_POST['username']); //获取用户输入的用户名
      $data["password"] = md5($_POST["password"]); //获取用户输入的密码
     	
      $arr=find('admin','*',$data);
     
      if($arr){
           $_SESSION["username"]=$arr["username"]; //将用户名保存在session中
           $_SESSION["nk"]=$arr["nk"]; 
           if(!empty($arr['level'])){
                 $_SESSION['level']=json_decode($arr['level']);
           }
          
           if(isset($_POST["remember"])){ //利用isset()判断不用empty()判断remember是否勾选
                setcookie("username",$arr["username"],time()+60*60); //将用户名保存到cookie中
                setcookie("level",$arr["level"],time()+60*60);
                setcookie("nk",$arr["nk"],time()+60*60);
           }
         
            jump('index/index');
      }else{
          echo "<script>alert('用户名密码错误')</script>";
      }

    }
    view('admin','login','index',array(),false);

}

function loginout(){
     //销毁session
    unset($_SESSION["username"]);
    setcookie("username","",0);
    //销毁之后跳转到登录页面
    header("location:index.php?m=admin&c=login&a=index");

}

function getvcode(){
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        if(strtoupper( $_POST['vcode'])==strtoupper($_SESSION["VerifyCode"])){
            echo 1;
        }else{
            echo 0;
        }
    }else{
         vCode();
    }
   

}

function getmail(){
    if(!empty($_POST['mail'])){
       $mail=$_POST['mail'];
      // $mail="ysfzf@.com.com";
        $pattern='/\w+@{1}\w+\.{1}\w+/';
        if(preg_match($pattern,$mail)){
             $total=total('admin',array('mail'=>$mail));
             if($total>0){
                echo 1;
             }else{
                echo '此邮箱不存在';
             }
        }else{
            echo '不合法的邮箱';
        }
       
    }else{
        echo '请输入您的邮箱';
    }
    die;
}

function forget(){
   
    $msg="";
    if(!empty($_POST)){
        $url="http://". $_SERVER["SERVER_NAME"]. $_SERVER['PHP_SELF'] ;
        $mail=$_POST['mail'];
        $row=find('admin','username',array('mail'=>$mail));
        if($row){
            $_POST['username']=$row['username'];
            $_POST['checkcode']=rand(100000,999999);
            $_POST['time']=time();
            $_POST['checkurl']=md5( $_POST['time']. $_POST['checkcode']);
            $url=$url.'?m=admin&c=login&a=getpwd&check='.$_POST['checkurl'];
            if(insert('forgetpwd')){
                $body="<p>您申请找回密码，验证码是：{$_POST['checkcode']}</p>";
                $body.="<p>此验证码两个小时内有效，点击以下网址或者复制此网址在浏览器中打开：</p>";
                $body.="<p><a href='$url' >$url</a></p>";
                $body.="<hr>";
                $body.="<p>".date('Y-m-d H:i:s',$_POST['time'])."</p>";
               // echo $body;die;
                sendMail($mail,$body);
                $msg="找回密码邮件已经发送，请打开您的邮箱再操作！";
            }
        }
        
      
    }
    view('admin','login','forget',array('msg'=>$msg),false);
}

function getpwd(){
    $msg='';
    if(!empty($_POST)){
        if(!empty($_GET['check'])){
            $check=$_GET['check'];
            $row=find('forgetpwd','',array('checkurl'=>$check));
            if($row){
                delete('forgetpwd',array('checkurl'=>$check));
                if((time()-$row['time'])<2*60*60 && ($_POST['code']==$row['checkcode'])&&($_POST['password']==$_POST['repassword'])){
                    $_POST['password']=md5($_POST['password']);
                   if(update('admin',array('username'=>$row['username']))){
                        jump('login/index');
                    }                    
                }
            }
        }
        $msg="重设密码失败！";
    }

    view('admin','login','getpwd',array('msg'=>$msg),false);

}