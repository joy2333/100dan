<?php

checkLogin();
function index(){  
   
	$data['title']='产品后台';
	if(isset($_GET['curpage'])){
		$data['curpage']=$_GET['curpage'];
	}else{
		$data['curpage']=1;
	}
    view('admin','product','index',$data);
}

function gettab(){
	$order='time Desc';
	$url="";
	if(!empty($_GET['order'])){
		$order='time Asc';
		$url="&order=asc";
	}

	$find=array();
	if(!empty($_GET['findstr'])){
		$find=array('content %'=>$_GET['findstr']);
		$url.="&findstr=".$_GET['findstr'];
	}

	$data= getpage('product', $order,5,5,$find);
	foreach($data['rows'] as $k=>$row){
		$category=find('category','name',$row['category']);
		$data['rows'][$k]['catname']=$category['name'];
	}
	$data['addurl']=$url;
    view('admin','product','gettab',$data,false);
}

function add(){
     
	if(isset($_POST['name'])){
		$_POST['time']=time();
		$imgarr=uploadImg('images', 'upload/product', $_POST['checkimages'],RES.'images/shuiyin.png');		 
		if(empty($imgarr['error'])){
			$_POST['images']=$imgarr['image'];
			$_POST['thumb']=$imgarr['thumb'];
			$_POST['water']=$imgarr['water'];
		}else{
			echo $imgarr['error'];
			die;
		}		
			 
		if(insert('product')){	 
			jump('index');			
		}else{
			echo "<script>alert('添加产品没有成功哦！');</script>";			
		}		
	}

	$category=select('category','id,name',array('pid'=>2))	;
	 view('admin','product','add',array('title'=>'添加产品','category'=>$category));
}

function edit(){
       
	if(isset($_GET['id'])){
		$id=$_GET['id'];
		$curpage=isset($_GET['curpage'])?$_GET['curpage']:1;
		$row=find('product','*',$id);
		if(isset($_POST['name'])&&is_numeric($id)){
			$imgarr=array();
			$thumbarr=array();
			$waterarr=array();
			
			//先上传添加的图片
			if(isset( $_POST['checkimages'])){			
				$imgs=uploadImg('images', 'upload/product', $_POST['checkimages'],RES.'images/shuiyin.png');		 
				if(empty($imgs['error'])){
					$imgarr= array_merge($imgarr,explode('|',$imgs['image']));
					$waterarr= array_merge($waterarr,explode('|',$imgs['water']));
					$thumbarr= array_merge($thumbarr,explode('|',$imgs['thumb']));
				}else{
					echo $imgs['error'];
					die;
				}			 
			}  
			//删除替换掉的图片
			$needimgarr=[];
			if(!empty($_POST['uploadedimages'])){
				$needimgarr=$_POST['uploadedimages'];
			}
			 
			if($row){
				$updimgarr=explode('|',$row['images']);
				$updthumbarr=explode('|',$row['thumb']);
				$updwaterarr=explode('|',$row['water']);
				for($i=0;$i<count($updimgarr);$i++){
					if(in_array($i,$needimgarr)){
						//修改后有此图片
						$imgarr[]=$updimgarr[$i];
						$waterarr[]=$updwaterarr[$i];
						$thumbarr[]= $updthumbarr[$i];
					}else{
						//修改后没有此图片
						if(file_exists($updimgarr[$i])){
							unlink($updimgarr[$i]);
						}
						if(file_exists($updwaterarr[$i])){
							unlink($updwaterarr[$i]);
						}
						if(file_exists($updthumbarr[$i])){
							unlink($updthumbarr[$i]);
						}

					}

				}

			}


			$_POST['images']=implode('|',$imgarr);
			$_POST['thumb']=implode('|',$thumbarr);
			$_POST['water']=implode('|',$waterarr);
			
			$result=update('product', $id);
			if($result){					
				jump('index',"curpage=$curpage");	
			}else{
				echo "<script>alert('修改没有成功哦！');</script>";			
			}
		}	
		
		if(count($row)>0){

			$data['row']=$row;
			$data['title']="编辑产品";
			$data['category']=select('category','id,name',array('pid'=>2))	;
			view('admin','product','edit',$data);
			
		}else{
			echo "<script>alert('没有查询到此ID的产品，可能已经删除了！');</script>";	
			header("location:index.php?m=admin&c=news&a=index&curpage=$curpage");
		}			
			
	}
}

function del(){
     
    if(isset($_GET['id'])){
        $id=$_GET['id'];
        $idarr=explode('|', $id);	
        $curpage=isset($_GET['curpage'])?$_GET['curpage']:1;
        
        foreach($idarr as $i){
            if(is_numeric($i)){		
                $mysql=find('product',$i);
                if($mysql){				 
                    unlink_imgs(explode('|',$mysql['images']));				 
                    unlink_imgs(explode('|',$mysql['thumb']));				 
                    unlink_imgs(explode('|',$mysql['water']));
                    delete('product',$i);
                }
                
                
            }
            
        }
	    jump('index',"curpage=$curpage");
    }
}

function checktop(){
	if(!empty($_POST)){
		$id=$_POST['id'];
		unset($_POST['id']);
		if(is_numeric($id)){
			if(update('product',$id)){
				echo "1";
			}else{
				echo "修改失败！";
			}
		}
	}

}
