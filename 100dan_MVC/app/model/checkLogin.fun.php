<?php
	/*判断是否登录*/
	function checkLogin(){
		
		if($GLOBALS["c"]=="login"){  //判断是否是登录页面
	  	    if(!empty($_SESSION["username"]) || !empty($_COOKIE["username"])){
				if(empty($_SESSION['username'])){
					$_SESSION['username']=$_COOKIE['username'];
					$_SESSION['nk']=$_COOKIE['nk'];
					if(!empty($_COOKIE['level'])){
						$_SESSION['level']=json_decode($_COOKIE['level']);
					}
				}

	           header("location:index.php?m=admin&c=news&a=index");
	        }
	     }else{
	
	         if(empty($_SESSION["username"]) && empty($_COOKIE["username"])){
	           
	           header("location:index.php?m=admin&c=login");
	
	       }else{
				if(empty($_SESSION['username'])){
					$_SESSION['username']=$_COOKIE['username'];
					$_SESSION['nk']=$_COOKIE['nk'];
					if(!empty($_COOKIE['level'])){
						$_SESSION['level']=json_decode($_COOKIE['level']);
					}
				}

		   }
	   }
		
				 
		
	}

	/*判断是否有此操作的权限*/
	function checkLevel(){
		global $c;
		global $a;
		global $m;
		if($m=="admin"){
			$action='index';			
			$arr=array('add','edit','del','show','editpwd','set','cset');// 不在此数组中的操作，只要有index操作权限就可以操作
			if(in_array($a,$arr)){
				$action=$a;
			}
			if(($c!='login') && ($c!='index')){
				$level=find('menu','id',array('module'=>$c,'action'=>$action,'app'=>$m));				
				if(!in_array($level['id'],$_SESSION['level'])){					
					jump("admin/index/error");								
				}
			}

		}
	}

	/*获取前台导航菜单*/
	function getSiteNav(){
		$nav=select('menu','',array('app'=>'site'));
		return $nav;

	}
