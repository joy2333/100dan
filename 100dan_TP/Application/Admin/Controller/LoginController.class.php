<?php
namespace Admin\Controller;
class LoginController extends PublicController{
	public function login(){


	   if(IS_POST){
		  $username= I("post.username");
		  $password =md5(I("post.password"));
		 
		  $res =M("admin")->where(array("username"=>$username,"password"=>$password))->find();
		  if(!empty($res)){
		  	 //不为空代表用户名密码输入正确
		       //$this->success("登录成功",U("news/index"),5);die;
		       //a$this->redirect("news/index",array(),5,"页面跳转中");
		     	//$this->redirect("index1",array("id"=>4,"page"=>2));
		     	//$this->redirect("home/index/index");
		     	//登录成功将用户名存储在session
		     	//如果用户点击勾选，则需要将信息存储在cookie
		     	session("username",$username);
		     	// if(!empty(I("post.remember"))){
				if(!empty(I("post.remember"))){
		     		  cookie("username",$username,1800);
		     	}
		  	     
		     	$this->redirect("news/index");
		   }else{
		   	  //代表登录失败
		   	  $this->error("登录失败",U("login/login"),5);die;
		   }
	    } 
		$this->display();//加载模板 模板渲染
	}
	
	
	public function loginout(){
		  session("username",null);
		  cookie("username",null);
		  $this->redirect("login");
	}

	public  function code(){
		if(IS_AJAX){
			 $this->check_verify();
		}
		$config = array(
            "length"=>4,
			"useNoise" => false,
			"useImgBg"=>false,
			"imageW"=>200,
			"imageH"=>60
		);
		$Verify = new \Think\Verify($config);
		$Verify->entry();
	}

	function check_verify(){
		$code = I("post.code");
		$verify = new \Think\Verify();
		if($verify->check($code)){
			 $this->ajaxReturn(1);
		}else{
			$this->ajaxReturn(0);
		}
	}
}