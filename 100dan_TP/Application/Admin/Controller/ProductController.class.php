<?php
namespace Admin\Controller;
class ProductController extends PublicController{
    public function index(){
        $count = M("product")->count();// 查询满足要求的总记录数
        $pagelist = 6; //每页显示的条数
        $Page = new \Think\Page($count,$pagelist);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $totalspage = $Page->totalPages; //总页码
        $currentpage = I("get.p","1");

        //查询每页显示的记录
        $list = M("product")->order("time DESC")->limit($Page->firstRow,$pagelist)->select();

        $this->assign("list",$list);
        $this->assign("totalspage",$totalspage);
        $this->assign("currentpage",$currentpage);


        $this->display();//加载模板并输出

    }

    public function insert(){
        if(IS_POST){
             $arr =  $this->upload(); //调用上传文件的函数 返回的是上传文件永久路径地址
             $_POST["images"]=$arr["images_path"];
             $_POST["thumb_images"]=$arr["thumb_path"];
             $_POST["water_images"]=$arr["water_path"];
             $_POST["time"]=time();

            $res =M("product")->data($_POST)->add();
            if($res>0){
                 $this->redirect("index");
            }else{
                 echo "<script>alert('新增失败')</script>";
            }

        }
          $this->display();
    }

     public function insert1(){ //多图上传
         if(IS_POST){
              $arr = $this->uploads();
             $_POST = array_merge($_POST,$arr);
             $_POST["time"]=time();

            $res = M("product")->add($_POST);
             if($res>0){
                 $this->redirect("index");
             }else{
                 echo "<script>alert('新增失败')</script>";
             }

         }

         $this->display();
     }
    public function upload(){
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize = 3145728 ;// 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath = './Uploads/'; // 设置附件上传根目录
        //注意：如果没有该Uploads文件夹则文件无法上传
        if(!file_exists($upload->rootPath )){
            mkdir($upload->rootPath,0777,true);
        }

        $upload->savePath = 'product/'; // 设置附件上传（子）目录 // 上传文件
        $info = $upload->upload();

        $images_path = $upload->rootPath.$info["images"]["savepath"].$info["images"]["savename"];

        $image = new \Think\Image();

        //生成缩略图：
        $image->open($images_path);// 打开原图
        $thumb_path = $upload->rootPath.$info["images"]["savepath"]."thumb_".$info["images"]["savename"];
        //代表缩略图的路径+名称
        $image->thumb(150, 150,3)->save($thumb_path);

        //水印图
        $image->open($images_path);// 打开原图
        $water_path  =  $upload->rootPath.$info["images"]["savepath"]."water_".$info["images"]["savename"];
        $image->water('shuiyin.png',9,50)->save($water_path);// 给原图添加水印并保存为water_o.gif（需要重新打开原图）


        return array("images_path"=>$images_path,"thumb_path"=>$thumb_path,"water_path"=>$water_path);
    }

    public function uploads(){
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize = 3145728 ;// 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath = './Uploads/'; // 设置附件上传根目录
        //注意：如果没有该Uploads文件夹则文件无法上传
        if(!file_exists($upload->rootPath )){
            mkdir($upload->rootPath,0777,true);
        }

        $upload->savePath = 'product/'; // 设置附件上传（子）目录 // 上传文件
        $info = $upload->upload();

        $images_path = ""; //表示上传文件的地址
        $thumb_path="";
        $water_path="";
        foreach($info as $v){
             $image_addr = $upload->rootPath.$v["savepath"].$v["savename"]; //上传的每张图片地址（代表一张地址）
             $images_path.= $image_addr."|";

            $image = new \Think\Image();
             //生成对应的缩略图
            $image->open($image_addr);// 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.jpg
            $thumb_addr = $upload->rootPath.$v["savepath"]."thumb_".$v["savename"];
            $image->thumb(150, 150)->save($thumb_addr);
            $thumb_path .=  $thumb_addr."|";

            //生成水印图
            $image->open($image_addr);
            $water_addr = $upload->rootPath.$v["savepath"]."water_".$v["savename"];
            $water_addr = $upload->rootPath.$v["savepath"]."water_".$v["savename"];
            $image->water("shuiyin.png")->save($water_addr);
            $water_path .= $water_addr."|";


        }

        $images_path =  substr($images_path,0,-1);
        $thumb_path =  substr($thumb_path,0,-1);
        $water_path =  substr($water_path,0,-1);
        return array("images"=>$images_path,"thumb_images"=>$thumb_path,"water_images"=>$water_path);

    }


    public function update(){
          $arr =array("w"=>"王昭君","d"=>"貂蝉","x"=>"西施");
         $this->assign("arr",$arr);
        $this->assign("id",8);
        $this->display();
    }
}