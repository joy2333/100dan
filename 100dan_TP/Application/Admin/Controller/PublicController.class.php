<?php
namespace Admin\Controller;
use Think\Controller;
class PublicController extends Controller{
	public function __construct(){
		parent::__construct();
		//只要是重写了controller的构造函数，则必须执行parent::__construct();
		//$this->checklogin();
	}
	public function checklogin(){
		 /*
		  *  判断是否是登录页面，如果是 ,继续判断，如果session和cookie有一个不为空 ，则代表已经登录，页面跳转新闻列表页
		  *  如果不是登录页面，判断session和cookie都为空，代表未登录，跳转到登录页面
		  */ 
		 if(ACTION_NAME!="code"){
			 if(ACTION_NAME == "login"){
				 if(session("?username") || !empty(cookie("username"))){
					 $this->redirect("news/index");
				 }
			 }else{
				 if(!session("?username") && empty(cookie("username"))) {
					 $this->redirect("login/login");
				 }

				 //如果session不存在，cookie存在则将cookie值赋值给session
				 if(!session("?username") && !empty(cookie("username"))) {
					 session("username", cookie("username"));
				 }
			 }
		 }
	}

	public function _empty($name){
		echo "您请求的空的操作:".$name;
	}

}