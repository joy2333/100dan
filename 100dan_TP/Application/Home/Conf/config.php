<?php
return array(
	//'配置项'=>'配置值'
    "TMPL_PARSE_STRING"=>array(
    	"__RESOURCE_PATH__"=>__ROOT__."/Application/Home/View/Public/", //新增一个模板替换
       
    ),
   	'LAYOUT_ON'=>true, //开启模板布局
    'LAYOUT_NAME'=>'public/layout',//设置布局的文件
	'SHOW_PAGE_TRACE' =>true,
	'LANG_SWITCH_ON'     =>     true,    //开启语言包功能
	'LANG_AUTO_DETECT'     =>     true, // 自动侦测语言
	'DEFAULT_LANG'         =>     'en-us', // 默认语言
	'LANG_LIST'            =>    'en-us', //必须写可允许的语言列表
	'VAR_LANGUAGE'     => '3', // 默认语言切换变量


);