<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>后台登录</title>
<base href="/Zend/100dan_10.06/Application/Admin/View/Public/"/>
<!--                       CSS                       -->
<!-- Reset Stylesheet -->
<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
<!-- Main Stylesheet -->
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />
<!--                       Javascripts                       -->
<!-- jQuery -->
<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
<!-- jQuery Configuration -->
<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
<!-- Facebox jQuery Plugin -->
<script type="text/javascript" src="resources/scripts/facebox.js"></script>
<!-- jQuery WYSIWYG Plugin -->
<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>

</head>
<body id="login">
<div id="login-wrapper" class="png_bg">
    <div id="login-top">
        <!-- <h1>Simpla Admin  /Zend/100dan_10.06/Public</h1> -->
        <!-- Logo (221px width) -->
        <a href="<?php echo U("index/index");?>"><img id="logo" src="resources/images/title.png" alt="后台管理系统" /></a> 
    </div>
  <!-- End #logn-top -->

    <div id="login-content">
      <form action="" method="post">
        <p>
            <label>用户</label>
            <input class="text-input" type="text" name="username" style="width:218px;float:left;"/>
        </p>
        <div class="clear"></div>
        <p>
            <label>密码</label>
            <input class="text-input" type="password" name="password" style="width:218px;float:left;"/>
        </p>
          <div class="clear"></div>
          <p>
              <label>验证码</label>
              <input class="text-input" type="text" name="code" style="width:100px;float:left;"/>
              <img src="<?php echo U('login/code');?>" class="codeimage" style="width:100px;float:right;"/>
          </p>
          <div class="clear"></div>
          <p>
            <span class="code_error"></span>
          </p>
        <div class="clear"></div>
        <p id="remember-password"><input type="checkbox" name='remember' value="1"/>记住我   |  <a href="<?php echo U("user/changePwd");?>}">忘记密码？</a></p>
        <div class="clear"></div>
        <p><input class="button" type="submit" value="Sign In" /></p>
      </form>
    </div>
  <!-- End #login-content -->
</div>
<!-- End #login-wrapper -->
</body>
</html>
<script>
    $(".codeimage").click(function(){
       var time = new Date().getTime();
       var src = "<?php echo U('code',array('num'=>time));?>";
        //注意：如果在js中使用thinkphp的函数，并且函数中有变量，不能拼接，可以采用后期替换的方式进行处理
        src = src.replace("time",time);

          $(this).attr("src",src);
    })

    $("input[name='code']").blur(function(){
        checkCode();
    });
    //验证验证码
    function checkCode(){

         var code =   $("input[name='code']").val();
        $.post("<?php echo U('code');?>",{"code":code},function(msg){
           if(msg==0){
               $(".code_error").html('您输入的验证码有误');
               j++;
           }else{
               $(".code_error").html('');
           }
        })


    }
</script>