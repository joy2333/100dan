<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<base href="/Zend/100dan_10.09/Application/Admin/View/Public/"/>
<!-- <title>Simpla Admin by www.865171.cn</title> -->
<!--                       CSS                       -->
<!-- Reset Stylesheet -->
<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
<!-- Main Stylesheet -->
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />
<!--                       Javascripts                       -->
<!-- jQuery -->
<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
<!-- jQuery Configuration -->
<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
<!-- Facebox jQuery Plugin -->
<script type="text/javascript" src="resources/scripts/facebox.js"></script>
<!-- jQuery WYSIWYG Plugin -->
<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>
<!-- jQuery Datepicker Plugin -->
<script type="text/javascript" src="resources/scripts/jquery.datePicker.js"></script>
<script type="text/javascript" src="resources/scripts/jquery.date.js"></script>
</head>
<body>
<div id="body-wrapper">
  <!-- Wrapper for the radial gradient background -->
  <div id="sidebar">
    <div id="sidebar-wrapper">
      <!-- Sidebar with logo and menu -->
      <h1 id="sidebar-title"><a href="#">网站后台</a></h1>
      <!-- Logo (221px wide) -->
      <a href="#"><img id="logo" src="resources/images/title.png" alt="网站后台页面" /></a>
      <!-- Sidebar Profile links -->
      <div id="profile-links">当前访问用户:<a href="#" title="Edit your profile"><?php echo (session('username')); echo ($_GET['id']); ?></a><br />
        <br />
        <a href="#" title="View the Site">访问站点</a> | <a href="<?php echo U('login/loginout');?>" title="Sign Out">退出</a> </div>
      <ul id="main-nav">

        <!-- Accordion Menu -->
        <li> <a href="<?php echo U('index/index');?>" class="nav-top-item no-submenu">
          后台首页</a>
        </li>
        <li> <a href="#" class="nav-top-item current">
          新闻模块</a>
          <ul>
            <li><a class="current" href="<?php echo U('news/index');?>">新闻列表</a></li>
            <li><a href="<?php echo U('news/insert');?>">新闻新增</a></li>
          </ul>
        </li>
        <li><a href="#" class="nav-top-item">产品模块</a>
          <ul>
            <li><a class="current" href="<?php echo U('product/index');?>">产品列表</a></li>
            <li><a href="<?php echo U('product/insert');?>">产品新增</a></li>
          </ul>
        </li>
        <li> <a href="#" class="nav-top-item">banner图模块</a>
          <ul>
            <li><a class="current" href="<?php echo U('banner/index');?>">banner列表</a></li>
            <li><a href="<?php echo U('banner/insert');?>">banner新增</a></li>
          </ul>
        </li>
      </ul>
      <!-- End #main-nav -->
    </div>
  </div>
  <!-- End #sidebar -->
  <div id="main-content">
 
    <div class="content-box">
      <!-- Start Content Box -->
      <div class="content-box-header">
        <h3>新闻列表</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab">Table</a></li>
        </ul>
        <div class="clear"></div>
      </div>
      <!-- End .content-box-header -->
      <div class="content-box-content">

          <div class="mychange">
          <table>
            <thead>
              <tr>
                <th>
                  <input class="check-all" type="checkbox" />
                </th>
                <!-- <th>id  <?php echo W("Menu/menu");?></th> -->
                <th>id</th>                
                <th>标题</th>
                <th>作者</th>
                <th>时间</th>
                <th>编辑</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <td colspan="6">

                  <div class="pagination"> 
                      <a href="<?php echo U('news/index',array('p'=>1));?>" title="First Page">&laquo; First</a>
                      <?php if($currentpage > 1): ?><a href="<?php echo U('news/index',array('p'=>$currentpage-1));?>" title="Previous Page">&laquo; Previous</a><?php endif; ?>
                      <?php $__FOR_START_11767__=1;$__FOR_END_11767__=$totalspage;for($i=$__FOR_START_11767__;$i <= $__FOR_END_11767__;$i+=1){ if(($i) == $currentpage): ?><a href="<?php echo U('news/index',array('p'=>$i));?>" class="number current" title="<?php echo ($i); ?>"><?php echo ($i); ?></a> 
                          <?php else: ?>
                            <a href="<?php echo U('news/index',array('p'=>$i));?>" class="number" title="<?php echo ($i); ?>"><?php echo ($i); ?></a><?php endif; } ?>
                     <?php if($currentpage < $totalspage): ?><a href="<?php echo U('news/index',array('p'=>$currentpage+1));?>" title="Next Page">Next &raquo;</a><?php endif; ?>
                      <a href="<?php echo U('news/index',array('p'=>$totalspage));?>" title="Last Page">Last &raquo;</a>
                      
                  </div>
                  <!-- End .pagination -->
                  <div class="clear"></div>
                </td>
              </tr>
            </tfoot>
            <tbody>
            <?php if(is_array($list)): foreach($list as $key=>$v): ?><tr>
                <td>
                  <input type="checkbox" />
                </td>
                <td><?php echo ($v["id"]); ?></td>
                <td><a href="#" title="title"><?php echo (strtoupper(mb_substr($v["title"],0,4,"utf-8"))); ?></a></td>
                <td><?php echo ((isset($v["author"]) && ($v["author"] !== ""))?($v["author"]):"未定义"); ?></td>
                <td><?php echo (date("Y-m-d H:i:s",$v["time"])); ?></td>
                <td>
                  <!-- Icons -->
                  <a href="<?php echo u('update',array('id'=>$v['id']));?>" title="Edit">
                  <img src="resources/images/icons/pencil.png" alt="Edit" /></a> 
                  <a href="<?php echo u('delete',array('id'=>$v['id']));?>" title="Delete">
                  <img src="resources/images/icons/cross.png" alt="Delete" /></a> 
                </td>
              </tr><?php endforeach; endif; ?>
            </tbody>
          </table>
        </div>
        <!-- End #tab1 -->

      </div>
      <!-- End .content-box-content -->
    </div>
    <!-- End .content-box -->

<script>
  $(".pagination a").live("click",function(){
     var url = $(this).attr("href"); //请求的超链接地址
    $.get(url,function(msg){
         $(".mychange").html(msg);
    },"html");
      return false;
  })


</script>

    <div id="footer" style="float:right;margin-bottom:20px;"> <small></small>
      <!-- Remove this notice or replace it with whatever you want -->
      &#169; Copyright 2010 Your Company | Powered by <a href="http://www.kernel123.cn">Joy</a> | <a href="<?php echo U('index/index');?>">首页</a> </small> </div>
    <!-- End #footer -->
  </div>
  <!-- End #main-content -->
</div>
</body>
</html>