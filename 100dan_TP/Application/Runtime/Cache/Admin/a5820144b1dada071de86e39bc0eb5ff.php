<?php if (!defined('THINK_PATH')) exit();?>
<table>
    <thead>
    <tr>
        <th>
            <input class="check-all" type="checkbox" />
        </th>
        <th>id</th>
        <th>标题</th>
        <th>作者</th>
        <th>时间</th>
        <th>编辑</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="6">
            <div class="bulk-actions align-left">
                <select name="dropdown">
                    <option value="option1">Choose an action...</option>
                    <option value="option2">Edit</option>
                    <option value="option3">Delete</option>
                </select>
                <a class="button" href="#">Apply to selected</a>
            </div>
            <div class="pagination">
                <a href="<?php echo U('news/index',array('p'=>1));?>" title="First Page">&laquo; First</a>
                <?php if($currentpage > 1): ?><a href="<?php echo U('news/index',array('p'=>$currentpage-1));?>" title="Previous Page">&laquo; Previous</a><?php endif; ?>
                <?php $__FOR_START_18900__=1;$__FOR_END_18900__=$totalspage;for($i=$__FOR_START_18900__;$i <= $__FOR_END_18900__;$i+=1){ if(($i) == $currentpage): ?><a href="<?php echo U('news/index',array('p'=>$i));?>" class="number current" title="<?php echo ($i); ?>"><?php echo ($i); ?></a>
                        <?php else: ?>
                        <a href="<?php echo U('news/index',array('p'=>$i));?>" class="number" title="<?php echo ($i); ?>"><?php echo ($i); ?></a><?php endif; } ?>
                <?php if($currentpage < $totalspage): ?><a href="<?php echo U('news/index',array('p'=>$currentpage+1));?>" title="Next Page">Next &raquo;</a><?php endif; ?>
                <a href="<?php echo U('news/index',array('p'=>$totalspage));?>" title="Last Page">Last &raquo;</a>

            </div>
            <!-- End .pagination -->
            <div class="clear"></div>
        </td>
    </tr>
    </tfoot>
    <tbody>
    <?php if(is_array($list)): foreach($list as $key=>$v): ?><tr>
            <td>
                <input type="checkbox" />
            </td>
            <td><?php echo ($v["id"]); ?></td>
            <td><a href="#" title="title"><?php echo (strtoupper(mb_substr($v["title"],0,4,"utf-8"))); ?></a></td>
            <td><?php echo ((isset($v["author"]) && ($v["author"] !== ""))?($v["author"]):"未定义"); ?></td>
            <td><?php echo (date("Y-m-d H:i:s",$v["time"])); ?></td>
            <td>
                <!-- Icons -->
                <a href="#" title="Edit">
                    <img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                <a href="#" title="Delete">
                    <img src="resources/images/icons/cross.png" alt="Delete" /></a>
                <a href="#" title="Edit Meta">
                    <img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
            </td>
        </tr><?php endforeach; endif; ?>
    </tbody>
</table>