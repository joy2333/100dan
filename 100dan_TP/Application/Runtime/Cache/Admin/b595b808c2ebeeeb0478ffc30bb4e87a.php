<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<base href="/Zend/100dan_10.09/Application/Admin/View/Public/"/>
<!-- <title>Simpla Admin by www.865171.cn</title> -->
<!--                       CSS                       -->
<!-- Reset Stylesheet -->
<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
<!-- Main Stylesheet -->
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />
<!--                       Javascripts                       -->
<!-- jQuery -->
<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
<!-- jQuery Configuration -->
<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
<!-- Facebox jQuery Plugin -->
<script type="text/javascript" src="resources/scripts/facebox.js"></script>
<!-- jQuery WYSIWYG Plugin -->
<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>
<!-- jQuery Datepicker Plugin -->
<script type="text/javascript" src="resources/scripts/jquery.datePicker.js"></script>
<script type="text/javascript" src="resources/scripts/jquery.date.js"></script>
</head>
<body>
<div id="body-wrapper">
  <!-- Wrapper for the radial gradient background -->
  <div id="sidebar">
    <div id="sidebar-wrapper">
      <!-- Sidebar with logo and menu -->
      <h1 id="sidebar-title"><a href="#">网站后台</a></h1>
      <!-- Logo (221px wide) -->
      <a href="#"><img id="logo" src="resources/images/title.png" alt="网站后台页面" /></a>
      <!-- Sidebar Profile links -->
      <div id="profile-links">当前访问用户:<a href="#" title="Edit your profile"><?php echo (session('username')); echo ($_GET['id']); ?></a><br />
        <br />
        <a href="#" title="View the Site">访问站点</a> | <a href="<?php echo U('login/loginout');?>" title="Sign Out">退出</a> </div>
      <ul id="main-nav">

        <!-- Accordion Menu -->
        <li> <a href="<?php echo U('index/index');?>" class="nav-top-item no-submenu">
          后台首页</a>
        </li>
        <li> <a href="#" class="nav-top-item current">
          新闻模块</a>
          <ul>
            <li><a class="current" href="<?php echo U('news/index');?>">新闻列表</a></li>
            <li><a href="<?php echo U('news/insert');?>">新闻新增</a></li>
          </ul>
        </li>
        <li><a href="#" class="nav-top-item">产品模块</a>
          <ul>
            <li><a class="current" href="<?php echo U('product/index');?>">产品列表</a></li>
            <li><a href="<?php echo U('product/insert');?>">产品新增</a></li>
          </ul>
        </li>
        <li> <a href="#" class="nav-top-item">banner图模块</a>
          <ul>
            <li><a class="current" href="<?php echo U('banner/index');?>">banner列表</a></li>
            <li><a href="<?php echo U('banner/insert');?>">banner新增</a></li>
          </ul>
        </li>
      </ul>
      <!-- End #main-nav -->
    </div>
  </div>
  <!-- End #sidebar -->
  <div id="main-content">
 
<title>后台欢迎页</title>
<!-- End .clear -->
<div class="content-box">
  <!-- Start Content Box -->
  <div class="content-box-header">
    <h3>后台欢迎页</h3>
    <div class="clear"></div>
  </div>
  <!-- End .content-box-header -->
  <img src="resources/images/welcome.png" style="width:100%"/>
</div>
<!-- End .content-box-content -->

    <div id="footer" style="float:right;margin-bottom:20px;"> <small></small>
      <!-- Remove this notice or replace it with whatever you want -->
      &#169; Copyright 2010 Your Company | Powered by <a href="http://www.kernel123.cn">Joy</a> | <a href="<?php echo U('index/index');?>">首页</a> </small> </div>
    <!-- End #footer -->
  </div>
  <!-- End #main-content -->
</div>
</body>
</html>