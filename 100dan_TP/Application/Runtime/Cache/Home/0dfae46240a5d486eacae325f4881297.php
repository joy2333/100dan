<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/Zend/100dan_10.06/Application/Home/View/Public/"/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css" />
        <script src="js/jquery-1.11.0.js"></script>        
        <script src="js/bootstrap.js"></script>
        
    </head>
 

		<link rel="stylesheet" href="css/brand_introduction.css" />

		<script type="text/javascript" src="js/jquery.touchSwipe.min.js" ></script>

		<script src="js/lunbo.js"></script>

		<title>品牌介绍</title>

	</head>

	<body>

		<!--header-->
		<div class="header">
			<div class="container">
				<div class="header_top">
					<div class="login">
						<ul class="nav-pills">
							<li>
								<a href="<?php echo U('login/login');?>">登录</a>
							</li>
							<li>丨</li>
							<li>
								<a href="<?php echo U('user/register');?>">注册</a>
							</li>
						</ul>
					</div>
					<div class="car">
						<a href="<?php echo U('product/shop_cart');?>">
							<img src="images/index_img/car.png" /> 我的购物车
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--header end-->

		<!--nav-->
		<div class="main_nav">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				      </button>
						<div class="logo">
							<a href="<?php echo U('index/index');?>"><img src="images/index_img/logo.jpg" title="欢迎进入凯涛奇" /></a>
						</div>
					</div>
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo U('index/index');?>" class="active">首页</a></li>
							<li><a href="<?php echo U('about/brand_introduction') class="active";?>">品牌介绍</a></li>
							<li><a href="<?php echo U('news/news');?>">新闻中心</a></li>
							<li><a href="<?php echo U('product/pro_list');?>">产品中心</a></li>
							<li><a href="<?php echo U('contact/contact_us');?>">联系我们</a></li>
							<li><a href="<?php echo U('user/user');?>">会员中心</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--nav end-->



		<!--banner-->

		<div id="myCarousel" class="carousel slide">

		   <!-- 轮播（Carousel）指标 -->

		   <ol class="carousel-indicators">

		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

		      <li data-target="#myCarousel" data-slide-to="1"></li>

		      <li data-target="#myCarousel" data-slide-to="2"></li>

		   </ol>   

		   <!-- 轮播（Carousel）项目 -->

		   <div class="carousel-inner">

		      <div class="item active">

		         <img src="images/index_img/banner01.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner02.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner03.jpg">

		      </div>

		   </div>

		</div>

		<!--banner end-->

		

		<!--content-->

		<div class="content">

			<div class="container">
				<div class="title">
					<h3>品牌介绍</h3>
				</div>

				<div class="brand">
					<div class="row">
						<div class="col-md-7 col-lg-7 col-sm-12 left">
							<img src="images/brand_img/brand.jpg"/>
						</div>

						<div class="right" >
							<p>

								天津凯涛奇食品有限公司，创建于2002年，坐落于津京塘中心地带的天津武清经济技术开发区内，是研发和制造薯片、饼干、糕点等食品的生产企业。

								历经十二年的发展，凯涛奇迈着踏实稳健的步伐逐步发展壮大，先后建立了02厂、08厂、09厂、10厂和11厂五个分厂，总占地面积46000多平方米，

								目前在生产的优质生产线共计二十条。2012-2014年，公司先后购得毗邻的110亩、24亩产业用地，并陆续兴建，标志着凯涛奇产业的进一步扩大。

								目前，公司拥有各类中高级管理人才、技术人员上百名，员工1500余名；是傲立北方市场的独资民族品牌食品生产企业。

							</p>

							<p>

								公司旗下拥有“乐吧”“约定”等多个品牌，数百支产品。公司自成立之初研发的可可约定软曲奇、早餐饼干、早餐饼干三连包等产品经久不衰，占据烘焙饼干市场的大量份额；

								2009年公司自主研发的“乐吧”系列薯片投放市场后一鸣惊人，产品供不应求，深受广大消费者的青睐和好评。凯涛奇系列产品现已覆盖全国大部分地区，并在我国北方市场取得了明显的销售优势地位。

								2014年，经公司精心研发试制的“乐吧舒为猴头菇饼干”、“乐吧逍食饼干”重磅上市，产品凭借真材实料的选材、优良的烘焙工艺，成为炙手可热的销售商品。

								目前，公司已形成以武清为腹地，以天津为中心辐射周边区域的销售态势，在全国各大省市均建立了稳定的销售网络，全国各地销售网点均呈现旺盛的增长势头，产品深受广大消费者喜爱。	

							</p>
							<p>以蓝本管理为依托，树立钢铁制度，于2012年，专门成立了食品安全管理委员会，形成了组长、主任、厂长三级管理；班组、车间、厂内三级自查；厂检、厂评、安全委员会三级监查的品检格局，严格自检互检，确保产品零缺点。</p>

							<p>

								诚信是企业立足之本，凯涛奇相继被各专业非营销机构评选为：价格诚信单位、A级纳税企业、食品安全优秀企业、天津高新区“十一五”突出贡献百强企业、天津市民营企业【健康成长工程】依法纳税百强企业、

								天津市民营企业【健康成长工程】促进就业百强企业，在“质量之光”全国系列评选活动中被评选为2012-2013年度质量管理创新企业，并于2013年获得安全生产标准化企业认证。

							</p>

							<p>

								精准的市场定位，高效的传播方法，使乐吧品牌系列产品位居同类产品前列。公司先后在青海卫视、江苏卫视、天津卫视的大份额投播让品牌传播更加顺畅，自2011年起公司便与天津卫视建立了战略合作伙伴关系，

								2014年，凯涛奇重磅推出“乐吧舒为猴头菇饼干”“乐吧逍食饼干”，并双双冠名天津卫视大型职场竞技栏目《非你莫属》，及大型戏曲明星真人秀《国色天香》栏目，9月份，以“乐吧薯片”冠名江苏卫视《一起来笑吧》栏目，

								将“爱心”“快乐”的企业和产品主题一同传播给广大消费者，进一步展现了成熟的品牌形象与企业形象。

							</p>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!--content end-->

		


<!--footer-->
<div class="footer">
    <div class="container">
        <div class="foot">
            <h3>甜品时尚</h3>
            <p>@2015pd.ad com All right reverved [山西甜品时尚]站长设计</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food,</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food.when it is dawn. </p>
        </div>
        <div class="two-code">
            <p>wdbeta dsign & Plwer by</p>
            <img src="images/index_img/two-code.png" />
        </div>
    </div>
</div>
</body>

</html>