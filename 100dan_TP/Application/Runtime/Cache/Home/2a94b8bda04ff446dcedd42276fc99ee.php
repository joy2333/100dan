<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/100dan/100dan_10.09/Application/Home/View/Public/"/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css" />
        <script src="js/jquery-1.11.0.js"></script>        
        <script src="js/bootstrap.js"></script>
        
    </head>
 
		<link rel="stylesheet" href="css/index.css" />
		
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js" ></script>
		<script src="js/lunbo.js"></script>
		<title>首页</title>
	

	<body>
		<!--header-->
		<div class="header">
			<div class="container">
				<div class="header_top">
					<div class="login">
						<ul class="nav-pills">
							<li>
								<a href="<?php echo U('login/login');?>">登录</a>
							</li>
							<li>丨</li>
							<li>
								<a href="<?php echo U('register/register');?>">注册</a>
							</li>
						</ul>
					</div>
					<div class="car">
						<a href="<?php echo U('purchase/shop_cart');?>">
							<img src="images/index_img/car.png" /> 我的购物车
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--header end-->

		<!--nav-->
		<div class="main_nav">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				      </button>
						<div class="logo">
							<a href="<?php echo U('index/index');?>"><img src="images/index_img/logo.jpg" title="欢迎进入凯涛奇" /></a>
						</div>
					</div>
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo U('index/index');?>" class="active">首页</a></li>
							<li><a href="<?php echo U('about/brand_introduction');?>">品牌介绍</a></li>
							<li><a href="<?php echo U('news/news');?>">新闻中心</a></li>
							<li><a href="<?php echo U('product/pro_list');?>">产品中心</a></li>
							<li><a href="<?php echo U('contact/contact_us');?>">联系我们</a></li>
							<li><a href="<?php echo U('user/user');?>">会员中心</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--nav end-->

		<!--banner-->
		<div id="myCarousel" class="carousel slide">
			<!-- 轮播（Carousel）指标 -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
			<!-- 轮播（Carousel）项目 -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="images/index_img/banner01.jpg">
				</div>
				<div class="item">
					<img src="images/index_img/banner02.jpg">
				</div>
				<div class="item">
					<img src="images/index_img/banner03.jpg">
				</div>
			</div>
		</div>
		<!--banner end-->
		
		<!--food-->
		<div class="food">
			<div class="container">
				<div class="col-md-12 title">
					<h3>挑战舌尖上的味蕾</h3>
					<p>Challenge on the tip of the tongue taste buds</p>
				</div>
					<div id="myCarousel1" class="carousel slide clear">
					<!-- 轮播（Carousel）指标 -->
					<ol class="carousel-indicators line">
						<li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel1" data-slide-to="1"></li>
						<li data-target="#myCarousel1" data-slide-to="2"></li>
					</ol>
					<!-- 轮播（Carousel）项目 -->
					<div class="carousel-inner">
						<div class="item active">
							<div class="row">
							<ul class="pic">
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic01.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>
								</li>
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic02.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>					
								</li>
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic03.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>				
								</li>
							</ul>
						</div>
						</div>
						<div class="item">
							<div class="row">
							<ul class="pic">
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic01.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>
								</li>
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic02.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>					
								</li>
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic03.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>				
								</li>
							</ul>
						</div>
						</div>
						<div class="item">
							<div class="row">
							<ul class="pic">
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic01.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>
								</li>
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic02.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>					
								</li>
								<li class="col-md-4 col-sm-4 col-xs-4 ">
									<div class="foodpic">
										<a href="<?php echo U('product/pro_center');?>">
											<img src="images/index_img/foodpic03.jpg" />
										</a>
										<div class="pic_h">
											<a href="<?php echo U('product/pro_center');?>">
												<img src="images/index_img/hover.png"/>
											</a>
										</div>
									</div>
									<div class="word">
										<p>吃货的世界只求实惠营养<br>
										This is food<br>
										选择我们，共同发展<br>
										色泽什么的搭配、对比、让人有食欲<br>
										工艺严谨，口味讲究</p>
									</div>				
								</li>
							</ul>
						</div>
						</div>
					</div>
				</div>
				<div class="line_pic">
					<img src="images/index_img/line_03.jpg">
				</div>
			</div>
		</div>
		<!--food end-->
		
		<!--product-->
		<div class="product">
			<div class="container">
				<div class="col-md-12 title">
					<h3>首推产品</h3>
					<p>Products recommended</p>
				</div>				
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6 big">
						<a href="<?php echo U('product/pro_center');?>">
							<img class="img-responsive" src="images/index_img/product01.jpg" />
						</a>
						<div class="text">
							<div class="inslid">					
								<p>幸福就是你想的味道，每一种美食都有一个故事。</p>
								<p>美食，顾名思义就是美味的食物，贵的有山珍海味，便宜的有街边小吃。</p>
							</div>
						</div>
					</div>
					<ul class="col-md-6 col-sm-6 col-xs-6 small_pic">
							<li class="col-md-6 col-sm-6 col-xs-6">
								<a href="<?php echo U('product/pro_center');?>">
									<img class="img-responsive" src="images/index_img/product02.jpg" />
								</a>
							</li>
							<li class="col-md-6 col-sm-6 col-xs-6">
								<a href="<?php echo U('product/pro_center');?>">
									<img class="img-responsive nopd" src="images/index_img/product03.jpg" />
								</a>
							</li>
							<li class="col-md-6 col-sm-6 col-xs-6">
								<a href="<?php echo U('product/pro_center');?>">
									<img class="img-responsive" src="images/index_img/product04.jpg" />
								</a>
							</li>
							<li class="col-md-6 col-sm-6 col-xs-6">
								<a href="<?php echo U('product/pro_center');?>">
									<img class="img-responsive" src="images/index_img/product05.jpg" />
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
		<!--product end-->
		
		<!--news-->
		<div class="news">
			<div class="container">
				<div class="col-md-12 title">
					<h3>我们的新闻</h3>
					<p>We have a news</p>
				</div>				
				<div class="row">
					<div class="col-md-6 col-sm-6 ">
						<a href="<?php echo U('news/news_details');?>">
							<img class="img-responsive" src="images/index_img/news.jpg" />
						</a>
					</div>
					<div class="col-md-6 col-sm-6 news-center">
						<div class="time">
							<h3>7月<small>13</small></h3>
						</div>
						<ul>
							<li>
								<a href="<?php echo U('news/news_details');?>">
									说假话的骄傲看到后看少打了少年了解了空间啊了解了了看爱上了爱上了
								</a>	
							</li>
							<li>
								<a href="<?php echo U('news/news_details');?>">
									立刻就涉及到了开始就看见了就看见了上课了看菱纱
								</a>	
							</li>
							<li>
								<a href="<?php echo U('news/news_details');?>">
									说假话的骄傲看到后看少打了少年了解了空间啊了解了了看爱上了爱上了
								</a>	
							</li>
							<li>
								<a href="<?php echo U('news/news_details');?>">
									立刻就涉及到了开始就看见了就看见了上课了看菱纱
								</a>	
							</li>
							<li>
								<a href="<?php echo U('news/news_details');?>">
									说假话的骄傲看到后看少打了少年了解了空间啊了解了了看爱上了爱上了
								</a>	
							</li>
							<li>
								<a href="<?php echo U('news/news_details');?>">
									立刻就涉及到了开始就看见了就看见了上课了看菱纱
								</a>	
							</li>
						</ul>
						<p><a href="<?php echo U('news/news');?>">更多></a></p>
					</div>
				</div>
			</div>
		</div>
		<!--news end-->
		
		
<!--footer-->
<div class="footer">
    <div class="container">
        <div class="foot">
            <h3>甜品时尚</h3>
            <p>@2015pd.ad com All right reverved [山西甜品时尚]站长设计</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food,</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food.when it is dawn. </p>
        </div>
        <div class="two-code">
            <p>wdbeta dsign & Plwer by</p>
            <img src="images/index_img/two-code.png" />
        </div>
    </div>
</div>
</body>

</html>