<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/100dan/100dan_10.09/Application/Home/View/Public/"/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css" />
        <script src="js/jquery-1.11.0.js"></script>        
        <script src="js/bootstrap.js"></script>
        
    </head>
 
		<link rel="stylesheet" href="css/news.css" />

		<script type="text/javascript" src="js/jquery.touchSwipe.min.js" ></script>

		<script src="js/lunbo.js"></script>

		<title>新闻中心</title>

	</head>

	<body>

		<!--header-->
		<div class="header">
			<div class="container">
				<div class="header_top">
					<div class="login">
						<ul class="nav-pills">
							<li>
								<a href="<?php echo U('login/login');?>">登录</a>
							</li>
							<li>丨</li>
							<li>
								<a href="<?php echo U('register/register');?>">注册</a>
							</li>
						</ul>
					</div>
					<div class="car">
						<a href="<?php echo U('purchase/shop_cart');?>">
							<img src="images/index_img/car.png" /> 我的购物车
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--header end-->

		<!--nav-->
		<div class="main_nav">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				      </button>
						<div class="logo">
							<a href="<?php echo U('index/index');?>"><img src="images/index_img/logo.jpg" title="欢迎进入凯涛奇" /></a>
						</div>
					</div>
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo U('index/index');?>">首页</a></li>
							<li><a href="<?php echo U('about/brand_introduction');?>">品牌介绍</a></li>
							<li><a href="<?php echo U('news/news');?>" class="active">新闻中心</a></li>
							<li><a href="<?php echo U('product/pro_list');?>">产品中心</a></li>
							<li><a href="<?php echo U('contact/contact_us');?>">联系我们</a></li>
							<li><a href="<?php echo U('user/user');?>">会员中心</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--nav end-->



		<!--banner-->

		<div id="myCarousel" class="carousel slide">

		   <!-- 轮播（Carousel）指标 -->

		   <ol class="carousel-indicators">

		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

		      <li data-target="#myCarousel" data-slide-to="1"></li>

		      <li data-target="#myCarousel" data-slide-to="2"></li>

		   </ol>   

		   <!-- 轮播（Carousel）项目 -->

		   <div class="carousel-inner">

		      <div class="item active">

		         <img src="images/index_img/banner01.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner02.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner03.jpg">

		      </div>

		   </div>

		</div>

		<!--banner end-->

		

		<!--content-->

		<div class="content">

			<div class="container">

				<div class="row">
					<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 text">
						<div class="up">
							<span>新闻中心</span>
						</div>

						<div class="down">
							<ul>

								<li>

									<a class="on" href="<?php echo U('news/news');?>">行业新闻</a>

								</li>

								<li>

									<a href="###">产品动态</a>

								</li>

								<li>

									<a href="###">相关新闻</a>

								</li>

							</ul>
						</div>
					</div>

					<div class="col-lg-10 col-md-9 col-sm-9 news_list">
						<h3>行业新闻</h3>

						<ul>

							<li>

								<div class="col-md-3">

									<a href="<?php echo U('news/news_details');?>"><img class="img-responsive hidden-sm hidden-xs" src="images/news_img/pic.jpg" /></a>

								</div>

								<div class="list col-md-9 col-sm-12">

									<div class="time col-md-2 col-sm-3 col-xs-3">
										<span>23/7</span><br />

										<span>2016</span>
									</div>

									<p class="col-md-10 col-sm-9 col-xs-9">快乐大了理科数学绿色了空间阿萨德离开谁的大来看撒娇的地方i健康去编号电视上开始是开始看嘛是奥斯卡妈的离开教室登录帅呆了地方了开始客服 电视电视剧了离开裸考军队离开飞科技了解了解是没你事仨... </p>

									<a href="<?php echo U('news/news_details');?>" class="pull-right more">更多>></a>

								</div>

							</li>

							<li>

								<div class="col-md-3">

									<a href="<?php echo U('news/news_details');?>"><img class="img-responsive hidden-sm hidden-xs" src="images/news_img/pic.jpg" /></a>

								</div>

								<div class="list col-md-9 col-sm-12">

									<div class="time col-md-2 col-sm-3 col-xs-3">

										<span>23/7</span><br />

										<span>2016</span>

									</div>

									<p class="col-md-10 col-sm-9 col-xs-9">快乐大了理科数学绿色了空间阿萨德离开谁的大来看撒娇的地方i健康去编号电视上开始是开始看嘛是奥斯卡妈的离开教室登录帅呆了地方了开始客服 电视电视剧了离开裸考军队离开飞科技了解了解是没你事仨... </p>

									<a href="<?php echo U('news/news_details');?>" class="pull-right more">更多>></a>

								</div>

							</li>

							<li>

								<div class="col-md-3">

									<a href="<?php echo U('news/news_details');?>"><img class="img-responsive hidden-sm hidden-xs" src="images/news_img/pic.jpg" /></a>

								</div>

								<div class="list col-md-9 col-sm-12">

									<div class="time col-md-2 col-sm-3 col-xs-3">

										<span>23/7</span><br />

										<span>2016</span>

									</div>

									<p class="col-md-10 col-sm-9 col-xs-9">快乐大了理科数学绿色了空间阿萨德离开谁的大来看撒娇的地方i健康去编号电视上开始是开始看嘛是奥斯卡妈的离开教室登录帅呆了地方了开始客服 电视电视剧了离开裸考军队离开飞科技了解了解是没你事仨... /p>

									<a href="<?php echo U('news/news_details');?>" class="pull-right more">更多>></a>

								</div>

							</li>

							<li>

								<div class="col-md-3">

									<a href="<?php echo U('news/news_details');?>"><img class="img-responsive hidden-sm hidden-xs" src="images/news_img/pic.jpg" /></a>

								</div>

								<div class="list col-md-9 col-sm-12">

									<div class="time col-md-2 col-sm-3 col-xs-3">

										<span>23/7</span><br />

										<span>2016</span>

									</div>

									<p class="col-md-10 col-sm-9 col-xs-9">快乐大了理科数学绿色了空间阿萨德离开谁的大来看撒娇的地方i健康去编号电视上开始是开始看嘛是奥斯卡妈的离开教室登录帅呆了地方了开始客服 电视电视剧了离开裸考军队离开飞科技了解了解是没你事仨... </p>

									<a href="<?php echo U('news/news_details');?>" class="pull-right more">更多>></a>

								</div>

							</li>

							<li>

								<div class="col-md-3">

									<a href="<?php echo U('news/news_details');?>"><img class="img-responsive hidden-sm hidden-xs" src="images/news_img/pic.jpg" /></a>

								</div>

								<div class="list col-md-9 col-sm-12">

									<div class="time col-md-2 col-sm-3 col-xs-3">

										<span>23/7</span><br />

										<span>2016</span>

									</div>

									<p class="col-md-10 col-sm-9 col-xs-9">快乐大了理科数学绿色了空间阿萨德离开谁的大来看撒娇的地方i健康去编号电视上开始是开始看嘛是奥斯卡妈的离开教室登录帅呆了地方了开始客服 电视电视剧了离开裸考军队离开飞科技了解了解是没你事仨... </p>

									<a href="<?php echo U('news/news_details');?>" class="pull-right more">更多>></a>

								</div>

							</li>

						</ul>

						<nav class="page">

						  <ul class="pagination pagination-sm">

						    <li><a href="#">&lsaquo;</a></li>

						    <li><a href="#" class="active">1</a></li>

						    <li><a href="#">2</a></li>

						    <li><a href="#">3</a></li>

						    <li>●●●●●●</li>

						    <li><a href="#">30</a></li>

						    <li><a href="#">&rsaquo;</a></li>

						  </ul>

						</nav>
					</div>

				</div>

			</div>

		</div>

		<!--content end-->

		

		
<!--footer-->
<div class="footer">
    <div class="container">
        <div class="foot">
            <h3>甜品时尚</h3>
            <p>@2015pd.ad com All right reverved [山西甜品时尚]站长设计</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food,</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food.when it is dawn. </p>
        </div>
        <div class="two-code">
            <p>wdbeta dsign & Plwer by</p>
            <img src="images/index_img/two-code.png" />
        </div>
    </div>
</div>
</body>

</html>