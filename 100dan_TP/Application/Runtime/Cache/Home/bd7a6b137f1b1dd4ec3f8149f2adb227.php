<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/100dan/100dan_10.09/Application/Home/View/Public/"/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css" />
        <script src="js/jquery-1.11.0.js"></script>        
        <script src="js/bootstrap.js"></script>
        
    </head>
 

		<link rel="stylesheet" href="css/pro_list.css" />

		<script type="text/javascript" src="js/jquery.touchSwipe.min.js" ></script>

		<script src="js/lunbo.js"></script>

		<title>产品中心</title>

	<body>

		<!--header-->
		<div class="header">
			<div class="container">
				<div class="header_top">
					<div class="login">
						<ul class="nav-pills">
							<li>
								<a href="<?php echo U('login/login');?>">登录</a>
							</li>
							<li>丨</li>
							<li>
								<a href="<?php echo U('register/register');?>">注册</a>
							</li>
						</ul>
					</div>
					<div class="car">
						<a href="<?php echo U('purchase/shop_cart');?>">
							<img src="images/index_img/car.png" /> 我的购物车
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--header end-->

		<!--nav-->
		<div class="main_nav">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				      </button>
						<div class="logo">
							<a href="<?php echo U('index/index');?>"><img src="images/index_img/logo.jpg" title="欢迎进入凯涛奇" /></a>
						</div>
					</div>
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo U('index/index');?>">首页</a></li>
							<li><a href="<?php echo U('about/brand_introduction');?>">品牌介绍</a></li>
							<li><a href="<?php echo U('news/news');?>">新闻中心</a></li>
							<li><a href="<?php echo U('product/pro_list');?>" class="active">产品中心</a></li>
							<li><a href="<?php echo U('contact/contact_us');?>">联系我们</a></li>
							<li><a href="<?php echo U('user/user');?>">会员中心</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--nav end-->



		<!--banner-->

		<div id="myCarousel" class="carousel slide">

		   <!-- 轮播（Carousel）指标 -->

		   <ol class="carousel-indicators">

		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

		      <li data-target="#myCarousel" data-slide-to="1"></li>

		      <li data-target="#myCarousel" data-slide-to="2"></li>

		   </ol>   

		   <!-- 轮播（Carousel）项目 -->

		   <div class="carousel-inner">

		      <div class="item active">

		         <img src="images/index_img/banner01.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner02.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner03.jpg">

		      </div>

		   </div>

		</div>

		<!--banner end-->

		

		<!--content-->

		<div class="content">

			<div class="container">

				<div class="row">
					<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 text">
						<div class="up">
							<span>产品中心</span>
						</div>

						<div class="down">
							<ul>

								<li>

									<a class="on" href="pro_list.html">乐吧薯片</a>

								</li>

								<li>

									<a href="###">精美糕点</a>

								</li>

								<li>

									<a href="###">舒为逍食</a>

								</li>

							</ul>
						</div>
					</div>

					<div class="col-lg-10 col-md-9 col-sm-9 main_pro">

						<div class="title">

							<h3>乐吧薯片</h3>

						</div>

						<ul>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 col-xs-6">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

							<li class="pro_list col-md-4 col-sm-4 hidden-xs">

								<div class="list">

									<a href="<?php echo U('product/pro_center');?>">

										<img class="img-responsive" src="images/pro_list_img/pro_list.jpg" />

									</a>

									<div class="word">

										<p>3321人付款</p>

										<p>&yen;2.5</p>

										<p><a href="<?php echo U('product/pro_center');?>">凯涛奇乐吧硬脆薯片（芝士味）</a></p>

										<p>50g/支</p>

										<p>★</p>

									</div>

									<a href="shop_cart.html" class="add">

										<img src="images/pro_list_img/car.png">

										加入购物车

									</a>

								</div>

							</li>

						</ul>

						<nav class="page">

						  <ul class="pagination">

						    <li><a href="###">&lsaquo;</a></li>

						    <li><a href="###" class="active">1</a></li>

						    <li><a href="###">2</a></li>

						    <li><a href="###">3</a></li>

						    <li>●●●●●●</li>

						    <li><a href="###">30</a></li>

						    <li><a href="###">&rsaquo;</a></li>

						  </ul>

						</nav>

					</div>

				</div>

			</div>

		</div>

		<!--content end-->

		



<!--footer-->
<div class="footer">
    <div class="container">
        <div class="foot">
            <h3>甜品时尚</h3>
            <p>@2015pd.ad com All right reverved [山西甜品时尚]站长设计</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food,</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food.when it is dawn. </p>
        </div>
        <div class="two-code">
            <p>wdbeta dsign & Plwer by</p>
            <img src="images/index_img/two-code.png" />
        </div>
    </div>
</div>
</body>

</html>