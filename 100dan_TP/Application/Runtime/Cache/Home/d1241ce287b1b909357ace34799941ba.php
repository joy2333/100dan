<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/Zend/100dan_10.06/Application/Home/View/Public/"/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css" />
        <script src="js/jquery-1.11.0.js"></script>        
        <script src="js/bootstrap.js"></script>
        
    </head>
 

		<link rel="stylesheet" href="css/shop_cart.css" />

		<script type="text/javascript" src="js/shop_cart.js" ></script>

		<title>购物车</title>

	<body>

		<!--header-->
		<div class="header">
			<div class="container">
				<div class="header_top">
					<div class="login">
						<ul class="nav-pills">
							<li>
								<a href="<?php echo U('login/login');?>">登录</a>
							</li>
							<li>丨</li>
							<li>
								<a href="<?php echo U('register/register');?>">注册</a>
							</li>
						</ul>
					</div>
					<div class="car">
						<a href="<?php echo U('product/shop_cart');?>">
							<img src="images/index_img/car.png" /> 我的购物车
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--header end-->

		<!--nav-->
		<div class="main_nav">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				      </button>
						<div class="logo">
							<a href="<?php echo U('index/index');?>"><img src="images/index_img/logo.jpg" title="欢迎进入凯涛奇" /></a>
						</div>
					</div>
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo U('index/index');?>">首页</a></li>
							<li><a href="<?php echo U('about/brand_introduction');?>">品牌介绍</a></li>
							<li><a href="<?php echo U('news/news');?>">新闻中心</a></li>
							<li><a href="<?php echo U('product/pro_list');?>">产品中心</a></li>
							<li><a href="<?php echo U('contact/contact_us');?>">联系我们</a></li>
							<li><a href="<?php echo U('user/user');?>">会员中心</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--nav end-->



		<!--content-->

		<div class="content">

			<div class="container">

				<div class="locative">
					<p>您现在位置：我的购物车</p>

				</div>

				<div class="row main">
					<div class="col-md-12 top">
						<div class="col-md-6 shop_left">
							<label class="col-md-6 c-checkbox all">
								<input type="checkbox" />

								<span></span>

								全选
							</label>

							<p class="col-md-6">商品信息</p>
						</div>

						<div class="col-md-6 shop_right shop_hidden">

							<dl>
								<dd>单价（元）</dd>

								<dd>数量</dd>

								<dd>金额（元）</dd>

								<dd>操作</dd>

							</dl>
						</div>
					</div>

					<ul class="col-md-12 goods">

						<li class="list">
							<div class="col-md-6 main_left">
								<label class="c-checkbox">

									<input type="checkbox"/>

									<span></span>

								</label>

								<div class="product">
									<a href="pro_center.html"><img src="images/shop_cart_img/goods.jpg" /></a>

									<p><a href="pro_center.html">美味实惠好吃美味实惠好吃美味实惠美味实美味实惠好吃美味实惠好吃美味实惠好吃美味实惠好吃...</a></p>
								</div>
							</div>

							<div class="col-md-6">

								<div class="shop_right shop">

									<dl>

										<dd class="money">

											<p>34.00</p>

											<p>24.00</p>

										</dd>

										<dd class="num">

											<button type="button" class="btn-left" onclick="">-</button>

											<input type="text" value="1" class="prd_num" maxlength="2" />

											<button type="button" class="btn-right" onclick="">+</button>

										</dd>

										<dd class="money2">

											24.00

										</dd>

										<dd class="del">

											<a href="###">删除</a>

										</dd>

									</dl>

								</div>
							</div>

						</li>

						

						<li class="list">

							<div class="col-md-6 main_left">

								<label class="c-checkbox">

									<input type="checkbox" />

									<span></span>

								</label>

								<div class="product">

									<a href="pro_center.html"><img src="images/shop_cart_img/goods.jpg" /></a>

									<p><a href="pro_center.html">美味实惠好吃美味实惠好吃美味实惠美味实美味实惠好吃美味实惠好吃美味实惠好吃美味实惠好吃...</a></p>

								</div>

							</div>

							<div class="col-md-6">

								<div class="shop_right shop">

									<dl>

										<dd class="money">

											<p>34.00</p>

											<p>24.00</p>

										</dd>

										<dd class="num">

											<button type="button" class="btn-left" onclick="">-</button>

											<input type="text" value="1" class="prd_num" maxlength="2" />

											<button type="button" class="btn-right" onclick="">+</button>

										</dd>

										<dd class="money2">

											24.00

										</dd>

										<dd class="del">

											<a href="###">删除</a>

										</dd>

									</dl>

								</div>

							</div>

						</li>

						

						<li class="list">

							<div class="col-md-6 main_left">

								<label class="c-checkbox">

									<input type="checkbox" />

									<span></span>

								</label>

								<div class="product">

									<a href="pro_center.html"><img src="images/shop_cart_img/goods.jpg" /></a>

									<p><a href="pro_center.html">美味实惠好吃美味实惠好吃美味实惠美味实美味实惠好吃美味实惠好吃美味实惠好吃美味实惠好吃...</a></p>

								</div>

								

							</div>

							<div class="col-md-6">

								<div class="shop_right shop">

									<dl>

										<dd class="money">

											<p>34.00</p>

											<p>24.00</p>

										</dd>

										<dd class="num">

											<button type="button" class="btn-left" onclick="">-</button>

											<input type="text" value="1" class="prd_num" maxlength="2" />

											<button type="button" class="btn-right" onclick="">+</button>

										</dd>

										<dd class="money2">

											24.00

										</dd>

										<dd class="del">

											<a href="###">删除</a>

										</dd>

									</dl>

								</div>

							</div>

						</li>

						

						<li class="list">

							<div class="col-md-6 main_left">

								<label class="c-checkbox">

									<input type="checkbox" />

									<span></span>

								</label>

								<div class="product">

									<a href="pro_center.html"><img src="images/shop_cart_img/goods.jpg" /></a>

									<p><a href="pro_center.html">美味实惠好吃美味实惠好吃美味实惠美味实美味实惠好吃美味实惠好吃美味实惠好吃美味实惠好吃...</a></p>

								</div>	

							</div>

							<div class="col-md-6">

								<div class="shop_right shop">

									<dl>

										<dd class="money">

											<p>34.00</p>

											<p>24.00</p>

										</dd>

										<dd class="num">

											<button type="button" class="btn-left" onclick="">-</button>

											<input type="text" value="1" class="prd_num" maxlength="2" />

											<button type="button" class="btn-right" onclick="">+</button>

										</dd>

										<dd class="money2">

											24.00

										</dd>

										<dd class="del">

											<a href="###">删除</a>

										</dd>

									</dl>

								</div>

							</div>

						</li>
					</ul>

					

					<div class="col-md-12 acount">

						<ul class="col-md-6 col-sm-12 col-xs-12 acount_left">

							<li class="col-md-3 col-sm-4">

								<label class="c-checkbox all down_all">

									<input type="checkbox" />

									<span></span>全选

								</label>

							</li>

							<li class="col-md-3 col-sm-4 "><a href="###" class="select">删除</a></li>

							<li class="col-md-6 col-sm-4 "><a href="###">清除失效产品</a></li>

						</ul>

						<ul class="col-md-6 col-sm-12 col-xs-12 acount_right">

							<li>已选商品 <span class="check-num"> 0 </span> 件</li>

							<li>合计（不含运费）：</li>

							<li>¥<span class="all_money">0.00</span></li>

							<li><a href="confirm_order.html">结算</a></li>

						</ul>

					</div>
				</div>
			</div>

		</div>

		<!--content end-->



		
<!--footer-->
<div class="footer">
    <div class="container">
        <div class="foot">
            <h3>甜品时尚</h3>
            <p>@2015pd.ad com All right reverved [山西甜品时尚]站长设计</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food,</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food.when it is dawn. </p>
        </div>
        <div class="two-code">
            <p>wdbeta dsign & Plwer by</p>
            <img src="images/index_img/two-code.png" />
        </div>
    </div>
</div>
</body>

</html>