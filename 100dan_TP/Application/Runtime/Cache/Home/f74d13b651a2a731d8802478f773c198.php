<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/100dan/100dan_10.09/Application/Home/View/Public/"/>
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/common.css" />
        <script src="js/jquery-1.11.0.js"></script>        
        <script src="js/bootstrap.js"></script>
        
    </head>
 

		<link rel="stylesheet" href="css/contact_us.css" />

		<script type="text/javascript" src="js/jquery.touchSwipe.min.js" ></script>

		<script src="js/lunbo.js"></script>

		<title>联系我们</title>

	<body>

		<!--header-->
		<div class="header">
			<div class="container">
				<div class="header_top">
					<div class="login">
						<ul class="nav-pills">
							<li>
								<a href="<?php echo U('login/login');?>">登录</a>
							</li>
							<li>丨</li>
							<li>
								<a href="<?php echo U('register/register');?>">注册</a>
							</li>
						</ul>
					</div>
					<div class="car">
						<a href="<?php echo U('purchase/shop_cart');?>">
							<img src="images/index_img/car.png" /> 我的购物车
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--header end-->

		<!--nav-->
		<div class="main_nav">
			<div class="container">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				         <span class="icon-bar"></span>
				      </button>
						<div class="logo">
							<a href="<?php echo U('index/index');?>"><img src="images/index_img/logo.jpg" title="欢迎进入凯涛奇" /></a>
						</div>
					</div>
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo U('index/index');?>" class="active">首页</a></li>
							<li><a href="<?php echo U('about/brand_introduction');?>">品牌介绍</a></li>
							<li><a href="<?php echo U('news/news');?>">新闻中心</a></li>
							<li><a href="<?php echo U('product/pro_list');?>">产品中心</a></li>
							<li><a href="<?php echo U('contact/contact_us');?>">联系我们</a></li>
							<li><a href="<?php echo U('user/user');?>">会员中心</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--nav end-->



		<!--banner-->

		<div id="myCarousel" class="carousel slide">

		   <!-- 轮播（Carousel）指标 -->

		   <ol class="carousel-indicators">

		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

		      <li data-target="#myCarousel" data-slide-to="1"></li>

		      <li data-target="#myCarousel" data-slide-to="2"></li>

		   </ol>   

		   <!-- 轮播（Carousel）项目 -->

		   <div class="carousel-inner">

		      <div class="item active">

		         <img src="images/index_img/banner01.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner02.jpg">

		      </div>

		      <div class="item">

		         <img src="images/index_img/banner03.jpg">

		      </div>

		   </div>

		</div>

		<!--banner end-->

		

		<!--content-->

		<div class="content">

			<div class="container">										
					<div class="title">
						<h3>联系我们</h3>
					</div>

					<div class="col-md-4 map">
						<!--百度地图容器-->

	  					<div style="width:100%;height:296px;border:#ccc solid 1px;" id="dituContent"></div>

	  					<script type="text/javascript" src="http://api.map.baidu.com/api?key=&v=1.1&services=true"></script>

	  					<script type="text/javascript" src="js/map.js"></script>
					</div>

					<div class="col-md-8 message">

						<form action="###">

							<div class="input-group">

								<span class="input-group-addon">姓名：</span>

								<input type="text" class="form-control">

							</div>

							<div class="input-group">

								<span class="input-group-addon">电话：</span>

								<input type="text" class="form-control">

							</div>

							<div class="input-group">

								<span class="input-group-addon">邮箱：</span>

								<input type="text" class="form-control">

							</div>

							<textarea></textarea>

							<div class="headup">

								<input type="submit" value="发送信息" />

								<input type="reset" value="重置信息" />

							</div>

						</form>

					</div>

					<div class="mes">

						<div class="col-md-6 col-sm-4">
							<p>联系我们</p>

							<p>电话：400-858-0698   传真：0755-8612 9326</p>
						</div>

						<div class="col-md-3 col-sm-4">
							<p>微信公众号</p>

							<p>asdhdbj</p>
						</div>

						<div class="col-md-3 col-sm-4">
							<p>邮箱</p>

							<p>asjdnwkdi@skd.net</p>
						</div>

						<div class="col-md-6 col-sm-4 down">

							<p>公司地址</p>

							<p>地  址：浙江省 杭州市 萧山区北干街道天汇园3幢1702室</p>

						</div>

						<div class="col-md-3 col-sm-4 down">

							<p>QQ</p>

							<p>400-858-0698</p>

						</div>

					</div>
			</div>

		</div>

		<!--content end-->

		

<!--footer-->
<div class="footer">
    <div class="container">
        <div class="foot">
            <h3>甜品时尚</h3>
            <p>@2015pd.ad com All right reverved [山西甜品时尚]站长设计</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food,</p>
            <p>when it is dawn, you can eat food, which means to iftar. when it is dawn, you can eat food.when it is dawn. </p>
        </div>
        <div class="two-code">
            <p>wdbeta dsign & Plwer by</p>
            <img src="images/index_img/two-code.png" />
        </div>
    </div>
</div>
</body>

</html>