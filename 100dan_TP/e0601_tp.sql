/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : e0601_joy

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-10-17 10:21:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `nk` varchar(20) NOT NULL,
  `password` char(32) NOT NULL,
  `email` varchar(30) NOT NULL COMMENT '邮箱',
  `level` varchar(255) NOT NULL,
  `status` int(4) NOT NULL,
  `time` int(20) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT '' COMMENT '角色',
  `addr` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('19', 'admin', 'Joy', '60a282f2f102d68a76b07e675da00483', '2622917057@qq.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"2\",\"11\",\"12\",\"13\",\"14\",\"3\",\"15\",\"16\",\"17\",\"18\",\"4\",\"19\",\"20\",\"21\",\"22\",\"5\",\"23\",\"24\",\"25\",\"6\",\"26\",\"27\",\"28\",\"29\",\"30\",\"31\",\"33\",\"35\",\"36\",\"42\"]', '0', '0', '系统管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('20', 'admin_news', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joy17702733325@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', '新闻管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('21', 'admin_product', 'Joy', '60a282f2f102d68a76b07e675da00483', 'as18956789@163.com', '[\"2\",\"11\",\"12\",\"13\",\"14\",\"35\",\"36\",\"42\"]', '0', '0', '产品管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('22', 'admin_banner', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joy13296664212@icloud.com', '[\"30\",\"31\",\"33\",\"35\",\"36\",\"42\"]', '0', '0', 'banner图管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('23', 'admin_category', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joy17702733325@gmail.com', '[\"3\",\"15\",\"16\",\"17\",\"18\",\"35\",\"36\",\"42\"]', '0', '0', '分类管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('24', 'admin_article', 'Joy', '60a282f2f102d68a76b07e675da00483', '17702733325@163.com', '[\"6\",\"26\",\"27\",\"28\",\"29\",\"35\",\"36\",\"42\"]', '0', '0', '文案管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('25', 'admin_message', 'Joy', '60a282f2f102d68a76b07e675da00483', '13296664212@163.com', '[\"5\",\"23\",\"24\",\"25\",\"35\",\"36\"]', '0', '0', '留言管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('26', 'admin_user', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joy13296664212@163.com', '[\"4\",\"19\",\"20\",\"21\",\"22\",\"35\",\"36\",\"42\"]', '0', '0', '用户管理员', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('27', '某某某', 'Joy', '60a282f2f102d68a76b07e675da00483', 'abcde1234@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', '公司联系人', '河北省槐安东路1399号', '400-800-8820');
INSERT INTO `admin` VALUES ('28', 'b', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joyb13296664212@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', 'bbbb', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('29', 'c', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joyc13296664212@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', 'cccc', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('30', 'd', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joyd13296664212@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', 'dddd', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('31', 'e', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joye13296664212@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', 'eeee', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('32', 'f', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joyf13296664212@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', 'ffff', '湖北武汉', '17702733325');
INSERT INTO `admin` VALUES ('33', 'g', 'Joy', '60a282f2f102d68a76b07e675da00483', 'joyg13296664212@163.com', '[\"1\",\"7\",\"8\",\"9\",\"10\",\"35\",\"36\",\"42\"]', '0', '0', 'gggg', '湖北武汉', '17702733325');

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `images` varchar(255) CHARACTER SET utf8 NOT NULL,
  `time` int(11) NOT NULL,
  `name` varchar(225) CHARACTER SET utf8 NOT NULL,
  `location` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('37', 'upload/banner/201709/15059879805656banner_3.jpg', '1505987980', 'banner3_index', 'index');
INSERT INTO `banner` VALUES ('35', 'upload/banner/201709/15059878582973banner_2.jpg', '1505987858', 'banner2_index', 'index');
INSERT INTO `banner` VALUES ('33', 'upload/banner/201709/15059878288834banner_1.jpg', '1505987828', 'banner1_index', 'index');
INSERT INTO `banner` VALUES ('50', 'upload/banner/201709/15062146512251pro_details_banner.jpg', '1506214246', 'banner_product_details', 'product_details');
INSERT INTO `banner` VALUES ('47', 'upload/banner/201709/15061025405293pro_banner.jpg', '1506102540', 'banner_product', 'product');
INSERT INTO `banner` VALUES ('48', 'upload/banner/201709/15061026175424news_banner.jpg', '1506102617', 'banner-news', 'news');
INSERT INTO `banner` VALUES ('46', 'upload/banner/201709/15061023929096about_banner.jpg', '1506102392', 'banner_about', 'about');
INSERT INTO `banner` VALUES ('49', 'upload/banner/201709/15061027114454contact_us_banner.jpg', '1506102711', 'banner_contact', 'contact');
INSERT INTO `banner` VALUES ('51', 'upload/banner/201709/15062146718599news_details_banner.jpg', '1506214283', 'banner_news_details', 'news_details');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '公司介绍', '0');
INSERT INTO `category` VALUES ('2', '新闻中心', '0');
INSERT INTO `category` VALUES ('3', '产品中心', '0');
INSERT INTO `category` VALUES ('4', '联系我们', '0');
INSERT INTO `category` VALUES ('5', '企业理念', '1');
INSERT INTO `category` VALUES ('6', '品牌文化', '1');
INSERT INTO `category` VALUES ('7', '公司荣誉', '1');
INSERT INTO `category` VALUES ('8', '服务理念', '1');
INSERT INTO `category` VALUES ('9', '关于我们', '0');
INSERT INTO `category` VALUES ('10', '行业资讯', '2');
INSERT INTO `category` VALUES ('11', '集团发展', '2');
INSERT INTO `category` VALUES ('12', '销量排行', '2');
INSERT INTO `category` VALUES ('13', '媒体报道', '2');
INSERT INTO `category` VALUES ('14', '产品介绍', '3');
INSERT INTO `category` VALUES ('15', '产品功能', '3');
INSERT INTO `category` VALUES ('16', '热门产品', '3');
INSERT INTO `category` VALUES ('17', '产品报价', '3');
INSERT INTO `category` VALUES ('18', '联系电话', '4');
INSERT INTO `category` VALUES ('19', '所在地址', '4');
INSERT INTO `category` VALUES ('20', '优秀团队', '4');
INSERT INTO `category` VALUES ('21', '加入我们', '4');
INSERT INTO `category` VALUES ('22', '橱柜一体', '16');
INSERT INTO `category` VALUES ('23', '衣帽间一体', '16');
INSERT INTO `category` VALUES ('24', '浴室柜一体', '16');
INSERT INTO `category` VALUES ('25', '人造石装饰', '16');

-- ----------------------------
-- Table structure for copy
-- ----------------------------
DROP TABLE IF EXISTS `copy`;
CREATE TABLE `copy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `category` int(10) unsigned NOT NULL,
  `content` text NOT NULL,
  `time` int(10) NOT NULL,
  `images` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of copy
-- ----------------------------
INSERT INTO `copy` VALUES ('1', 'index_about', '首页公司介绍文案', '0', '<p>福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p><p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美...</p>', '1506065295', 'upload/about/201709/15060652954719about_us.jpg', '公司介绍0');
INSERT INTO `copy` VALUES ('3', 'index_product', '首页产品中心文案2', '0', '<p>2-简约而不简单，面板采用饰面纹路自然逼真的美极板装饰，与白色的墙面做出了明显的对比，在利用现代常用的装饰素材磨砂玻璃、不锈钢缔造出简约明快的厨房氛围...</p>', '1506092044', 'upload/copy/201709/15060920441633product_pic2.jpg', '无');
INSERT INTO `copy` VALUES ('2', 'index_product', '首页产品中心文案1', '0', '<p>1-门板采用大面积的咖啡色UV板，板上包覆的铝合金，线条感的动感十足。配置上奶白色的石英石台面，就如同在您劳累一天，停下来的休息时刻，冲泡出一杯香味浓郁的咖啡...</p>', '1506091480', 'upload/copy/201709/15060914801198product_pic1.jpg', '无');
INSERT INTO `copy` VALUES ('4', 'index_product', '首页产品中心文案3', '0', '<p>3-浴室柜的面材可分为天然石材、人造石材、防火板、烤漆、玻璃、金属和实木等；基材是浴室柜的主体，它被面材所掩饰。基材是浴室柜品质和价格的决定因素...</p>', '1506092430', 'upload/copy/201709/15060924304238product_pic3.jpg', '无');
INSERT INTO `copy` VALUES ('5', 'index_product', '首页产品中心文案4', '0', '<p>4-充满希望的季节，反逆向的把冷色的色调展示出来。以强烈的对比，以冷碰冷的色调，把我们的家居表现出来。色彩是冷艳的，通过环境的衬托...</p>', '1506092480', 'upload/copy/201709/15060924804166product_pic4.jpg', '无');
INSERT INTO `copy` VALUES ('6', 'index_news', '首页新闻中心文案1', '0', '1-新版有道翻译官,黑科技和实用性功能并存 有道翻译官是网易有道2012年推出的.....', '1506093188', 'upload/copy/201709/15060931882902news_pic1.jpg', '1.家居行业普遍喊冷，为何上市公司业绩普遍飘红');
INSERT INTO `copy` VALUES ('7', 'index_news', '首页新闻中心文案2', '0', '2-新版有道翻译官,黑科技和实用性功能并存 有道翻译官是网易有道2012年推出的.....', '1506093473', 'upload/copy/201709/15060934736434news_pic2.jpg', '2.家居行业普遍喊冷，为何上市公司业绩普遍飘红');
INSERT INTO `copy` VALUES ('8', 'index_news', '首页新闻中心文案3', '0', '3-新版有道翻译官,黑科技和实用性功能并存 有道翻译官是网易有道2012年推出的.....', '1506093558', 'upload/copy/201709/15060935589520news_pic3.jpg', '3.家居行业普遍喊冷，为何上市公司业绩普遍飘红');
INSERT INTO `copy` VALUES ('35', 'about', 'about页文案', '0', '<p>0-福宝·京华整体厨房成立于2000年，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美化厨房为己任。在引导和满足顾客消费的同时不断发展壮大，公司现有员工百余人，专业人才济济，组织机构健全，以董事会为核心下设：业务销售部、专业设计师组成的设计部、采购供应部、财务审核部、生产部、人造石石业部、质检部、安装部、售后服务部。</p>\r\n<p>福宝·京华整体厨房面对充满机遇和挑战的21世纪，公司将坚持不断创新、持续发展的拼搏精神，锐意进取，进一步实现人才结构优化、管理水平和技术含量提升，加快科学、现代、国际化的规模经营步伐。为人类生活品质的不断提高做出积极的贡献。</p>\r\n<p>福宝·京华整体厨房成立于2000年，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美化厨房为己任。在引导和满足顾客消费的同时不断发展壮大，公司现有员工百余人，专业人才济济，组织机构健全，以董事会为核心下设：业务销售部、专业设计师组成的设计部、采购供应部、财务审核部、生产部、人造石石业部、质检部、安装部、售后服务部。</p>', '1506167524', 'upload/copy/201709/15061675249300about_us_pic.jpg', '公司简介0');
INSERT INTO `copy` VALUES ('36', 'pro_details', '产品中心详情页文案', '0', '<label>0-基本信息：</label>\r\n<table>\r\n    <tr>\r\n        <td>柜体环保级别：E1</td>\r\n        <td>台面材质：石英石</td>\r\n        <td>台板厚度：20mm</td>\r\n    </tr>\r\n    <tr>\r\n        <td>型号：蓝调</td>\r\n        <td>柜板是否进口：否</td>\r\n        <td>形状：L形</td>\r\n    </tr>\r\n    <tr>\r\n        <td>计价单位：1延米</td>\r\n        <td>保修时长：1年</td>\r\n        <td>柜体材质：其他</td>\r\n    </tr>\r\n    <tr>\r\n        <td>门板材质：亚克力</td>\r\n        <td>颜色分类：3米套餐</td>\r\n        <td>同城服务：送货上门并安装</td>\r\n    </tr>\r\n</table>\r\n<label>特点：</label>\r\n<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', '1506213387', 'upload/copy/201709/15062133877442pro_details_pic.jpg', '0-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做');
INSERT INTO `copy` VALUES ('37', 'news_details', '新闻中心详情页', '0', '<p>0-齐鲁晚报03月13日讯：3月9日-3月12日，2017新加坡国际家具展览会（简称IFFS）在新加坡樟宜国际博览中心举办，该展会是专门针对出口欧美市场和覆盖东南亚市场的重要展览会，展示了全球家居市场的新产品和新概念，预示最新的行业趋势。酷家乐携多项新技术参加本次展会，凭借着独创的快速渲染与全景VR技术，受到了国际客户的热捧。此次亮相具备国际影响力的IFFS，酷家乐用核心技术“征服”了来自世界各国的参观商与参展商。同时，除了交流技术、理念，掌握国际趋势外，酷，除了交流技术、理念，掌握国际趋势外，酷家乐有意瞄准需求旺盛的东南亚市场。</p>\r\n<p>IFFS创办于1981年，有新加坡家具理事会举办，每年一届，以展品齐全，专业性程度高著称。展会展出各类型家具、设计作品、家居饰品，以及行业科技等。上届展会吸引了来自50000家参展企业，客商数量达到90000人。</p>\r\n<p>本届展会展出面积近8万平米，吸引了超过100多个国家的采购商与会，盛况空前，被称为国际平台上最富有成效的家具展之一。展会旨在展示国际家居行业创</p>\r\n<p>酷家乐是本次展会中唯一一家立足于家居设计环节的科技型企业。由于其技术的领先性、独创性与稀缺性，在展会现场，酷家乐展区吸引了来自美国、发过、丹麦、日本、印度、泰国、马来西亚、越南以及新加坡等各国参展商的来访，纷纷咨询运营模式，并亲身体验酷家乐的快速渲染和全景VR技术。酷家乐商务部副总</p>\r\n<p>放眼国际，家居行业都在经历着一场消费需求大升级，业主对个性化需求强烈，并具备较强水平的审美能力，这也刺激着设计端的变革。而专注于家居设计环节的科技型企业并不多见，发展成熟者寥寥，国际市场依然存在较大的市场空白点。酷家乐是全球屈指可数的具备该类技术国际竞争力的平台，其技术领先性已经超越了国外水平，这也是其多次获得GGV、IDG、赫斯特等国际性资本青睐的根源。</p>\r\n<p>酷家乐是本次展会中唯一一家立足于家居设计环节的科技型企业。由于其技术的领先性、独创性与稀缺性，在展会现场，酷家乐展区吸引了来自美国、发过、丹麦、日本、印度、泰国、马来西亚、越南以及新加坡等各国参展商的来访，纷纷咨询运营模式，并亲身体验酷家乐的快速渲染和全景VR技术。酷家乐商务部副总裁廖溪表示，在与访客交流的过程中，听说最多的评价是“impossible”、“amazing”、“fantastic”。</p>\r\n<div>\r\n    <a href=\"#\" id=\"prev\">上一篇：董易林老师受邀参加名佳·悠季家具新品发布会？</a>\r\n    <a href=\"#\" id=\"next\">下一篇：董易林老师受邀参加名佳·悠季家具新品发布会？</a>\r\n</div>', '1506213573', 'upload/copy/201709/15062135739110news_details_pic.jpg', '0-2017新加坡国际家具展开幕 中国企业酷家乐受热捧');

-- ----------------------------
-- Table structure for forgetpwd
-- ----------------------------
DROP TABLE IF EXISTS `forgetpwd`;
CREATE TABLE `forgetpwd` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `checkcode` char(6) NOT NULL,
  `checkurl` varchar(32) NOT NULL,
  `time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of forgetpwd
-- ----------------------------
INSERT INTO `forgetpwd` VALUES ('1', 'admin', '171191', 'cab8c1580873764fe49c14c9a941574e', '1502878612');
INSERT INTO `forgetpwd` VALUES ('2', 'admin', '242163', '5dd79cb2d1c84898be279aee89f7c31f', '1502878658');
INSERT INTO `forgetpwd` VALUES ('3', 'admin', '598559', '74677b83cde89946b7dd51b512b1a0d8', '1502878733');
INSERT INTO `forgetpwd` VALUES ('4', 'admin', '117550', '37b8fd1f852bb3320be019de69e336b1', '1503184645');
INSERT INTO `forgetpwd` VALUES ('5', 'admin', '903649', '556fac6c4e4488f64d3aa31e8edbf66a', '1503185841');
INSERT INTO `forgetpwd` VALUES ('6', 'admin', '626684', '50182d496584d4e9e76662b6c892efb1', '1503185898');
INSERT INTO `forgetpwd` VALUES ('12', 'admin', '654754', 'cc1a94ae6b113d712e326b9bb87d3431', '1503221311');
INSERT INTO `forgetpwd` VALUES ('13', 'admin', '747946', 'ed75a2debc9138bf403caec5096febb2', '1503221331');
INSERT INTO `forgetpwd` VALUES ('14', 'admin', '654260', '541e9cd3e1d408722caa930e9feb0a86', '1503221643');
INSERT INTO `forgetpwd` VALUES ('15', 'admin', '145538', 'afc9b27093fa1c6c082bcbfad90c62a7', '1503285448');
INSERT INTO `forgetpwd` VALUES ('16', 'admin', '313711', 'ab0033b5f0dab162da0a144ddd139bfa', '1503306942');
INSERT INTO `forgetpwd` VALUES ('17', 'admin', '270535', '9f7b28c39e030acd185e950d36f1d9ec', '1505184474');

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `time` int(4) unsigned DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of level
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `module` varchar(20) NOT NULL,
  `action` varchar(20) NOT NULL,
  `app` varchar(20) NOT NULL,
  `hidden` int(1) unsigned NOT NULL DEFAULT '0',
  `pid` int(5) unsigned NOT NULL DEFAULT '0',
  `submenu` varchar(20) NOT NULL DEFAULT '',
  `ename` varchar(50) NOT NULL COMMENT '英文名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '新闻中心', 'news', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('2', '产品中心', 'product', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('3', '分类', 'category', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('4', '用户', 'admin', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('5', '留言', 'message', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('6', '介绍文案', 'about', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('7', '新闻列表', 'news', 'index', 'admin', '1', '1', '模块', '');
INSERT INTO `menu` VALUES ('8', '添加新闻', 'news', 'add', 'admin', '1', '1', '模块', '');
INSERT INTO `menu` VALUES ('9', '编辑新闻', 'news', 'edit', 'admin', '1', '1', '模块', '');
INSERT INTO `menu` VALUES ('10', '删除新闻', 'news', 'del', 'admin', '1', '1', '模块', '');
INSERT INTO `menu` VALUES ('11', '产品列表', 'product', 'index', 'admin', '1', '2', '模块', '');
INSERT INTO `menu` VALUES ('12', '添加产品', 'product', 'add', 'admin', '1', '2', '模块', '');
INSERT INTO `menu` VALUES ('13', '编辑产品', 'product', 'edit', 'admin', '1', '2', '模块', '');
INSERT INTO `menu` VALUES ('14', '删除产品', 'product', 'del', 'admin', '1', '2', '模块', '');
INSERT INTO `menu` VALUES ('15', '分类列表', 'category', 'index', 'admin', '1', '3', '模块', '');
INSERT INTO `menu` VALUES ('16', '添加分类', 'category', 'add', 'admin', '1', '3', '模块', '');
INSERT INTO `menu` VALUES ('17', '编辑分类', 'category', 'edit', 'admin', '1', '3', '模块', '');
INSERT INTO `menu` VALUES ('18', '删除分类', 'category', 'del', 'admin', '1', '3', '模块', '');
INSERT INTO `menu` VALUES ('19', '用户列表', 'admin', 'index', 'admin', '1', '4', '模块', '');
INSERT INTO `menu` VALUES ('20', '添加用户', 'admin', 'add', 'admin', '1', '4', '模块', '');
INSERT INTO `menu` VALUES ('21', '编辑用户', 'admin', 'edit', 'admin', '1', '4', '模块', '');
INSERT INTO `menu` VALUES ('22', '删除用户', 'admin', 'del', 'admin', '1', '4', '模块', '');
INSERT INTO `menu` VALUES ('23', '留言列表', 'message', 'index', 'admin', '1', '5', '模块', '');
INSERT INTO `menu` VALUES ('24', '留言详情', 'message', 'show', 'admin', '1', '5', '模块', '');
INSERT INTO `menu` VALUES ('25', '删除留言', 'message', 'del', 'admin', '1', '5', '模块', '');
INSERT INTO `menu` VALUES ('26', '文案列表', 'about', 'index', 'admin', '1', '6', '模块', '');
INSERT INTO `menu` VALUES ('27', '添加文案', 'about', 'add', 'admin', '1', '6', '模块', '');
INSERT INTO `menu` VALUES ('28', '编辑文案', 'about', 'edit', 'admin', '1', '6', '模块', '');
INSERT INTO `menu` VALUES ('29', '删除文案', 'about', 'del', 'admin', '1', '6', '模块', '');
INSERT INTO `menu` VALUES ('30', 'Banner图', 'banner', 'index', 'admin', '0', '0', '模块', '');
INSERT INTO `menu` VALUES ('31', 'Banner列表', 'banner', 'index', 'admin', '1', '30', '模块', '');
INSERT INTO `menu` VALUES ('33', '编辑Banner', 'banner', 'edit', 'admin', '1', '30', '模块', '');
INSERT INTO `menu` VALUES ('35', '个人信息', 'admin', 'set', 'admin', '0', '0', '设置', '');
INSERT INTO `menu` VALUES ('36', '修改密码', 'admin', 'editpwd', 'admin', '0', '0', '设置', '');
INSERT INTO `menu` VALUES ('37', '首页', 'index', 'index', 'site', '1', '0', '站点', 'Home');
INSERT INTO `menu` VALUES ('38', '关于我们', 'about', 'index', 'site', '1', '0', '站点', 'About us');
INSERT INTO `menu` VALUES ('39', '新闻中心', 'news', 'index', 'site', '1', '0', '站点', 'News Center');
INSERT INTO `menu` VALUES ('40', '产品中心', 'product', 'index', 'site', '1', '0', '站点', 'Product Center');
INSERT INTO `menu` VALUES ('41', '联系我们', 'contact', 'index', 'site', '1', '0', '站点', 'Contact Us');
INSERT INTO `menu` VALUES ('42', '公司信息', 'admin', 'cset', 'admin', '0', '0', '设置', '');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `tel` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('3', 'ff', '10086', '', '联表查询：几张表联查\n 内联 ：INNER JOIN  取交集\n特点：1 ：取交集，两张表对应的数据必须都存在还会显示\n          2：必须设置关联条件，关联条件用ON  不能用where\n          3：联表查询查询中如果出现字段重复，则后面字段将前面字段覆盖，\n        4：如果字段重复，设置筛选条件时则需要指定是哪张表中的字段', '1502694011');
INSERT INTO `message` VALUES ('4', 'tt', '13800138', 'ysfzf@hotmail.com', 'var $name=$(\"input[name=\'name\']\").val();\n    var $tel=$(\"input[name=\'tel\']\").val();\n    var $email=$(\"input[name=\'email\']\").val();\n    var $message=$(\"textarea\").val();', '1502694154');
INSERT INTO `message` VALUES ('6', 'joy', '17702733325', '2622917057@qq.com', '加油！棒棒哒 ！~(≧3≦)~', '1505205248');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(10) unsigned NOT NULL,
  `title` varchar(60) NOT NULL,
  `content` varchar(255) NOT NULL,
  `time` int(10) NOT NULL,
  `hits` int(10) unsigned NOT NULL,
  `author` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `top` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否首页显示',
  `location` varchar(255) NOT NULL COMMENT '新闻简介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('2', '0', '2-2017新加坡国际家具展开幕 中国企业酷家乐受热捧？', '<p><a href=\"index.php?m=site&c=news&a=details\">2-齐鲁晚报03月13日讯：3月9日-3月12日，2017新加坡国际家具展览会（简称IFFS）在新加坡樟宜国际博览中心举办,该展会是专门针对出口欧美市场和覆盖东南亚市场的重要展览会，展示了全球家居市场的新产品和新概念，预示最新的行业趋势。酷家乐携多项新技术参加本次展会...</a></p>', '1506136153', '0', 'joy', 'upload/news/201709/15061340101652news_pic5.jpg', '0', 'news');
INSERT INTO `news` VALUES ('1', '0', '1-2017新加坡国际家具展开幕 中国企业酷家乐受热捧？', '<p><a href=\"index.php?m=site&c=news&a=details\">1-齐鲁晚报03月13日讯：3月9日-3月12日，2017新加坡国际家具展览会（简称IFFS）在新加坡樟宜国际博览中心举办,该展会是专门针对出口欧美市场和覆盖东南亚市场的重要展览会，展示了全球家居市场的新产品和新概念，预示最新的行业趋势。酷家乐携多项新技术参加本次展会...</a></p>', '1506136141', '0', 'joy', 'upload/news/201709/15061361417912news_pic4.jpg', '0', 'news');
INSERT INTO `news` VALUES ('3', '0', '3-2017新加坡国际家具展开幕 中国企业酷家乐受热捧？', '<p><a href=\"index.php?m=site&c=news&a=details\">3-齐鲁晚报03月13日讯：3月9日-3月12日，2017新加坡国际家具展览会（简称IFFS）在新加坡樟宜国际博览中心举办,该展会是专门针对出口欧美市场和覆盖东南亚市场的重要展览会，展示了全球家居市场的新产品和新概念，预示最新的行业趋势。酷家乐携多项新技术参加本次展会...</a></p>', '1506136163', '0', 'joy', 'upload/news/201709/15061338016509news_pic6.jpg', '0', 'news');
INSERT INTO `news` VALUES ('4', '0', '4-2017新加坡国际家具展开幕 中国企业酷家乐受热捧？', '<p><a href=\"index.php?m=site&c=news&a=details\">4-齐鲁晚报03月13日讯：3月9日-3月12日，2017新加坡国际家具展览会（简称IFFS）在新加坡樟宜国际博览中心举办,该展会是专门针对出口欧美市场和覆盖东南亚市场的重要展览会，展示了全球家居市场的新产品和新概念，预示最新的行业趋势。酷家乐携多项新技术参加本次展会...</a></p>', '1506136171', '0', 'joy', 'upload/news/201709/15061338206857news_pic7.jpg', '0', 'news');
INSERT INTO `news` VALUES ('5', '0', 'aa', '<p>aaa<br/></p>', '1506302263', '0', 'aa', 'upload/news/201709/15063022625470Chrysanthemum.jpg', '0', 'aa');
INSERT INTO `news` VALUES ('6', '0', 'bbbbbbbbbbbbbb', '<p>bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb</p>', '1506309487', '0', 'bbbbbbbbbbbb', 'upload/news/201709/15063094878378banner_2.jpg|upload/news/201709/15063094872916banner_3.jpg', '0', 'bbbbbb');
INSERT INTO `news` VALUES ('7', '0', 'cccccccccccccccc', '<p>cccccccccccccccccccccccccccccccccccccccccc</p>', '1506309508', '0', 'ccccccccccccc', 'upload/news/201709/15063095084779logo.jpg|upload/news/201709/15063095082334m_details_c_bk.png|upload/news/201709/15063095083407more_details_bk.png|upload/news/201709/15063095083856more_details_c_bk.png', '0', 'ccccc');
INSERT INTO `news` VALUES ('8', '0', 'eeeeeeee', '', '0', '0', '', '', '0', '');
INSERT INTO `news` VALUES ('9', '0', 'fffffffffffff', '', '0', '0', '', '', '0', '');
INSERT INTO `news` VALUES ('10', '0', 'ggggggggggggg', '', '0', '0', '', '', '0', '');
INSERT INTO `news` VALUES ('11', '0', 'hhhhhhhhhhh', '', '0', '0', '', '', '0', '');
INSERT INTO `news` VALUES ('12', '0', 'iiiiiiiiiiiii', '', '0', '0', '', '', '0', '');
INSERT INTO `news` VALUES ('13', '0', 'jjjjjjjjjjjjjjjjj', '', '0', '0', '', '', '0', '');
INSERT INTO `news` VALUES ('14', '0', 'kkkkkkkkkkkkkkk', '', '0', '0', '', '', '0', '');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(10) unsigned NOT NULL,
  `name` varchar(30) DEFAULT NULL COMMENT '产品名称',
  `location` varchar(255) NOT NULL COMMENT '产品介绍简写',
  `content` text COMMENT '产品介绍详情',
  `images` varchar(255) NOT NULL COMMENT '产品图片',
  `time` int(255) unsigned NOT NULL,
  `thumb` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `water` varchar(255) DEFAULT NULL COMMENT ' 水印图',
  `top` int(1) unsigned NOT NULL DEFAULT '0',
  `huanbao` varchar(255) DEFAULT NULL,
  `caizhi_taimian` varchar(255) DEFAULT NULL,
  `houdu` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `jinkou` varchar(255) DEFAULT NULL,
  `shape` varchar(255) DEFAULT NULL,
  `baoxiu` varchar(255) DEFAULT NULL,
  `caizhi_guiti` varchar(255) DEFAULT NULL,
  `caizhi_menban` varchar(255) DEFAULT NULL,
  `type_color` varchar(255) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub1_title` varchar(255) DEFAULT NULL,
  `sub2_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('8', '0', '32.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061402625892pro_pic3.jpg', '1506267106', 'upload/product/201709/thumb_15061402625892pro_pic3.jpg', 'upload/product/201709/water_15061402625892pro_pic3.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '32-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '32-基本信息', '32-特点');
INSERT INTO `product` VALUES ('5', '0', '22.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061714914950pro_pic2.jpg', '1506267084', 'upload/product/201709/thumb_15061714914950pro_pic2.jpg', 'upload/product/201709/water_15061714914950pro_pic2.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '22-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '22-基本信息', '22-特点');
INSERT INTO `product` VALUES ('6', '0', '23.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061714018329pro_pic2.jpg', '1506267091', 'upload/product/201709/thumb_15061714018329pro_pic2.jpg', 'upload/product/201709/water_15061714018329pro_pic2.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '23-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '23-基本信息', '23-特点');
INSERT INTO `product` VALUES ('2', '0', '12.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061624772379pro_pic1.jpg', '1506267059', 'upload/product/201709/thumb_15061624772379pro_pic1.jpg', 'upload/product/201709/water_15061624772379pro_pic1.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '12-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '12-基本信息', '12-特点');
INSERT INTO `product` VALUES ('3', '0', '13.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061624954269pro_pic1.jpg', '1506267067', 'upload/product/201709/thumb_15061624954269pro_pic1.jpg', 'upload/product/201709/water_15061624954269pro_pic1.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '13-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '13-基本信息', '13-特点');
INSERT INTO `product` VALUES ('1', '0', '11.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061624772379pro_pic1.jpg', '1506267059', 'upload/product/201709/thumb_15061624772379pro_pic1.jpg', 'upload/product/201709/water_15061624772379pro_pic1.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '11-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '11-基本信息', '11-特点');
INSERT INTO `product` VALUES ('4', '0', '21.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061714503826pro_pic2.jpg', '1506267076', 'upload/product/201709/thumb_15061714503826pro_pic2.jpg', 'upload/product/201709/water_15061714503826pro_pic2.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '21-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '21-基本信息', '21-特点');
INSERT INTO `product` VALUES ('7', '0', '31.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061402625892pro_pic3.jpg', '1506267099', 'upload/product/201709/thumb_15061402625892pro_pic3.jpg', 'upload/product/201709/water_15061402625892pro_pic3.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '31-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '31-基本信息', '31-特点');
INSERT INTO `product` VALUES ('9', '0', '33.欧式风格', 'product', '<p> 福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、优质创新、超越自我的企业精神，始终不渝的以提高人们生活品位，美      福宝·京华整体厨房成立于2000年，于2006年增资百万正式注册为河北福宝京华家居有限公司，是集销售、研发、设计、生产、安装、售后服务为一体的大型专业生产厂家。产品主营整体厨房、浴室柜、衣帽间、人造石装饰，同时还涉足办公家具、卫生间隔断、商场展台等。</p>\r\n<p>福宝·京华整体厨房自成立以来积极发扬团结协作、人们生活品位。</p>', 'upload/product/201709/15061402625892pro_pic3.jpg', '1506267114', 'upload/product/201709/thumb_15061402625892pro_pic3.jpg', 'upload/product/201709/water_15061402625892pro_pic3.jpg', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', '33-京华橱柜 整体橱柜定制石英石 现代简约 整体厨房厨柜装修定做', '33-基本信息', '33-特点');
INSERT INTO `product` VALUES ('303', '0', 'yyy', 'yyyy', '<p><img src=\"/ueditor/php/upload/image/20170925/1506337159102295.jpg\" title=\"1506337159102295.jpg\" alt=\"banner_1.jpg\"/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p>rtrrrr</p><p><br/></p><p><br/></p><p>reshrt</p><p><br/></p><p><br/></p><table><tbody><tr class=\"firstRow\"><td width=\"99\" valign=\"top\" style=\"word-break: break-all;\"><p>&#39;</p><p>{<br/><br/></p></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\" style=\"word-break: break-all;\"><p><br/></p><p><br/></p><p>]]]</p></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr><tr><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td><td width=\"99\" valign=\"top\"><br/></td></tr></tbody></table><p><br/></p>', 'upload/product/201709/15063371916635contact_code.png', '1506337191', 'upload/product/201709/thumb_15063371916635contact_code.png', 'upload/product/201709/water_15063371916635contact_code.png', '0', 'E1', '石英石', '20mm', '蓝调', '否', 'L形', '1年', '其他', '亚克力', '3米套餐', '送货上门并安装', '1延米', null, null, null);
